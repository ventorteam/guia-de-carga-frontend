/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.factory('resourceZona', function ($resource) {
    return {
        getZonaID: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getZonaID,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
         getZonaSinUbic: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getZonaSinUbic,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        getZonasSinDueno: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getZonasSinDueno,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        getAllZona: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getAllZonas,
                    isArray: false,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        postZonaDueno: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postDuenoZona,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
           postZona: function () {
               return $resource(api,{},{
                   ejecutar: {
                       method: 'POST',
                       url: dir.postZona,
                        transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    },
                       header: {'Content-Type': 'application/json'}
                   }
               });
           },
           postZonaUbicacion : function () {
               return $resource(api,{},{
                   ejecutar : {
                       method: 'POST',
                       url: dir.postZonaUbicacion,
                       header: {'Content-Type' : 'application/json'}
                   }
               });
           },
           deleteZona : function (index) {
               return $resource(api,{},{
                   ejecutar: {
                       method: 'DELETE',
                       url: dir.deleteZona + index
                   }
               })
           }
           // prueba : function () {
           //     return $resource('http://162.243.125.126/demos/hed_regional/index.php/venta/excel/04/2015',{},{
           //         ejecutar: {
           //             method: 'GET',
           //             url:     'http://162.243.125.126/demos/hed_regional/index.php/venta/excel/04/2015',
           //             header: {'Content-Type': 'application/json'}
           //         }
           //     })
           // }
    };

});
