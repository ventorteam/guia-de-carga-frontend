app.factory('resourceGuias', function ($resource) {
    return {
        getGuiasSinDespachar: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getGuiasSinDespachar,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        },
        getFacturasSinGuia: function (inicio, fin) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getFacturasSinGuia + inicio + '/' + fin + '/',
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        },
        getCamionesPorTransportista: function (transportista) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getCamionesPorTransportista + transportista,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        },
        postGuia: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postGuia,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        getGuiaID: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getGuiaID,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        updateGuia: function (guiaId) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateGuia + guiaId,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateFacturaE: function (factura) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateFacturaE + factura,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateFacturaD: function (factura) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateFacturaD + factura,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateFacturasinGuiaD: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateFacturaDSinGuia + guia,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateFacturasinGuiaA: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateFacturaESinGuia + guia,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        getCalculoCajaProductos: function (guia) {
            return  $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.calculoCajaProductos + guia,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        getReporteGuiaPortada: function (guia) {
            return  $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getReporteGuiaPortada + guia,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                }
            });
        },
        deleteGuia: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteGuia + guia
                }
            });
        },
        deleteFacturaDeGuiaA: function (Array) {
            return  $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.deleteFacturaDeGuiaA,
                    headers: {"Facturas": Array},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        deleteFacturaDeGuiaB: function (Array) {
            return  $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.deleteFacturaDeGuiaB,
                    headers: {"Facturas": Array},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateGuiaMod: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateGuiaMod + guia,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        }
    };

});