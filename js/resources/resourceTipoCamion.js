/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.factory('resourceTipoCamion', function ($resource) {
    return {
        getAllTipoCamiones: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getAllTipoCamiones,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        postTipoCamion: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postTipoCamion,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        
        
        updateTipoCamion: function (tipo) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateTipoCamion + tipo,
                    transformResponse: function(data, headersGetters, status){
                        return {content: data};
                    },
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        deleteTipoCamion: function (tipo) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteTipoCamion + tipo,
                    header: {"Content-Type": "application/json"}
                }
            });
        }

    };
});