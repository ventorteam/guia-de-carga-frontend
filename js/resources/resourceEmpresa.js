/**
 * Created by PROGRAMADOR on 09/08/2016.
 */
app.factory('resourceEmpresa', function ($resource) {
    return {
        getEmpresa: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getEmpresa,
                    isArray: false,
                    headers: {'Content-Type': 'application/json'}
                }
            });
        },

        postEmpresa: function(){
            return $resource(api,{},{
                ejecutar:{
                    method: 'POST',
                    url: dir.postEmpresa,
                    isArray: false,
                    headers: {'Content-Type': 'application/json'}
                }
            });
        },

        updateEmpresa: function(id){
            return $resource(api, {},{
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateEmpresa + id,
                    isArray: false,
                    headers: {'Content-Type': 'application/json'}
                }
            });
        },

        deleteEmpresa: function(id){
            return $resource(api, {}, {
                ejecutar:{
                    method: 'DELETE',
                    url: dir.deleteEmpresa + id,
                    isArray: false,
                    headers: {'Content-Type': 'application/json'}
                }
            });
        }
    }
});