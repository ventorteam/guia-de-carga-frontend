app.factory('resourceCiudades', function ($resource) {
    return {
        getCiudades: function (estado) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getCiudades+estado,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        }
    };
});