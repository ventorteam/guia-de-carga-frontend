app.factory('resourceGuiasDespachadas', function ($resource) {
    return {
        getGuiasDespachadas: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getGuiasDespachadas,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        getFacturaConGuiaE: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getFacturaConGuiaE + guia,
                    isArray: false,
                    header: {"Content-Type": "application/json"}

                }
            });
        },
        getUniCiudad: function (estado, ciudad) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getUniCiudad + estado + '/' + ciudad,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        getReporteGuiaPortada: function (ID) {
            return $resource(api, {}, {
                method: 'GET',
                url: dir.getReporteGuiaPortada + ID,
                isArray: false,
                header: {"Content-Type": "application/json"},
                transformResponse: function (data, headersGetter, status) {
                    return {content: data};
                }
            });
        },
        getCalculoFacturaGuia: function (ID) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getCalculoFacturaGuia + ID,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
         updateGuiaMotivo: function (idguia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateGuiaMotivo + idguia,
                    headers: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        },
        updateGuiaModT: function (ID) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateGuiaModT + ID,
                    isArray: false,
                    header: {"Content-Type": "application/json"},
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    }
                }
            });
        }
        
    };


});