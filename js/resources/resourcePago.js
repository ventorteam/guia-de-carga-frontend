/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 By: Jhon Simon */

app.factory('resourcePago', function ($resource) {
    return {
        getPagos: function (fechaInit, fechaFin) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getPagos + fechaInit + '/' + fechaFin,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        },
        getGuiasPagar: function (fechaInit, fechaFin, idTrans) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getGuiasPagar + fechaInit + '/' + fechaFin + '/' + idTrans + '/',
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        },
        postPago: function (guia) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postPago,
                    isArray: false,
                    headers: {"Guias": guia}
                }
            });
        },
        reportePago: function (idpago) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.reportePago + idpago,
                    isArray: false,
                    headers: {"Content-Type": 'application/json'}
                }
            });
        },
        deletePago: function (idPago) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deletePago + idPago,
                    isArray: false,
                    headers: {"Content-Type": 'application/json'}
                }
            });
        }

    };
});

