/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.factory('resourceDueno', function ($resource) {

    return {
        postDueno: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postDueno,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        
        getAllDuenos: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getAllDuenos,
                    isArray: false,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        
        updateDueno: function (id) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateDueno + id,
                    // isArray:false,
                    header: {'Content-Type': 'application/json'}
                }
            });

        },
        deleteDueno: function (id) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteDueno + id,
                    // isArray:false,

                }
            });
        },
        postDuenoZona: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postDuenoZona,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        getZonaConDueno: function (ci) {
            return $resource(api,{},{
                ejecutar: {
                    method: 'GET',
                    url: dir.getZonaDueno + ci,
                    header: {'Content-Type': 'application/json'}
                }
            });
        }
    }
});

