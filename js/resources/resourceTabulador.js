
app.factory('resourceTabulador', function($resource){
   return {
       
        getFtipo: function(estado, ciudad){
            return $resource(api, {},{
               ejecutar: {
                   method: 'GET',
                   url: dir.getFtipo + '/' + estado + '/' + ciudad,
                   isArray: false,
                   headers: {'Content-Type':'application/json'}
               } 
            });
        },
        
        postTipoF: function(){
            return $resource(api,{},{
               ejecutar:{
                   method: 'POST',
                   url: dir.postTipoF,
                   isArray: false,
                   transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    },
                   headers: {"Content-type":"application/json"}
               } 
            });
        },
        
        getFletes: function(){
            return $resource(api, {}, {
               ejecutar:{
                   method: 'GET',
                   url: dir.getFletes,
                   isArray: false,
                   headers:{"Content-type":"application/json"}
               } 
            });
        },
        
        updateFletes: function(estado, ciudad){
            return $resource(api,{},{
               ejecutar: {
                   method: 'PUT',
                   url: dir.updateFletes + '/' + estado + '/' + ciudad,
                   isArray: false,
                   headers: {"Content-Type":'application/json'}
               } 
            });
        },
        
        deleteFletes: function(estado,ciudad){
            return $resource(api, {},{
                ejecutar:{
                    method: 'DELETE',
                    url: dir.deleteFletes + '/' + estado + '/' + ciudad,
                    isArray: false,
                    headers: {"Content-Type": 'application/json'}
                }
            });
        },
       getEstadosTabulados: function(){
           return $resource (api, {},{
              ejecutar: {
                  method: 'GET',
                  url: dir.getEstadosTabulados,
                  isArray: false,
                  headers: {"Content-Type": 'application/json'}
              }
           });
       },
       getCiudadesTabuladas: function(codesta){
           return $resource (api, {},{
               ejecutar: {
                method: 'GET',
                   url: dir.getCiudadesTabuladas + codesta,
                   isArray: false,
                   headers: {"Content-Type":'application/json'}
               }
           });
       }
        
   }; 
});
