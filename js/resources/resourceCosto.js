/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.factory('resourceCosto', function ($resource) {
    return {
        getCostos: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getCostos,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }                          
            });
        },
         updateCostos: function(idCosto){
                    return $resource(api, {},{
                        ejecutar: {
                         method: 'PUT',
                         url: dir.updateCostos + idCosto,
                         isArray: false,
                         headers: {"Content-Type": "application/json"}
                        }
                    });
                }


    };

});
