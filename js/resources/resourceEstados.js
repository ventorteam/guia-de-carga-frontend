app.factory('resourceEstados', function ($resource) {
    return {
        getEstados: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getEstados,
                    isArray: false,
                    headers: {"Content-Type": "application/json"}
                }
            });
        }
    };
});