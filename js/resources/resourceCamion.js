/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.factory('resourceCamion', function ($resource) {
    return {
        getCamionesContransportista: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getCamionesContransportista,
                    isArray: false,
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        postCamion: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postCamion,
                    header: {"Content-Type": "application/json"}
//                      transformResponse: function (data, headersGetter, status) {
//                          
//                        return {content: data};
//                    }
                }
            });
        },
        updateCamion: function (placa) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateCamion + placa,
                    transformResponse: function (data, headersGetter, status) {
                        return {content: data};
                    },
                    header: {"Content-Type": "application/json"}
                }
            });
        },
        deleteCamion: function (placa) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteCamion + placa,
                    header: {"Content-Type": "application/json"}
                }
            });
        }

    };
});