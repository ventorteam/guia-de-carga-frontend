/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.factory('resourceMotivo', function ($resource) {
    return {
        getMotivo: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getMotivo,
                    isArray: false,
                    headers: {"Content-Type": 'application/json'}
                }
            });
        },
        postMotivo: function () {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.postMotivo,
                    isArray: false,
                    headers: {"Content-Type": 'application/json'}
                }
            });
        },
        
        deleteMotivo: function(idMotivo){
            return $resource (api,{},{
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteMotivo + idMotivo,
                    isArray: false,
                    headers: {"Content-Type":'application/json'}
                }
            });
        }
    };
});
