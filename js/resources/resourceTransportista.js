/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
app.factory('resourceTransportista', function ($resource) {

    return {
        setTransportista: function () {

            return $resource(api, {}, {
                ejecutar: {
                    method: 'POST',
                    url: dir.setTransportista,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        getAllTransportista: function () {

            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getAllTransportista,
                    isArray: false,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        
        getTransportistasCamion: function () {

            return $resource(api, {}, {
                ejecutar: {
                    method: 'GET',
                    url: dir.getTransportistasCamion,
                    isArray: false,
                    header: {'Content-Type': 'application/json'}
                }
            });
        },
        updateTransportista: function (id) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'PUT',
                    url: dir.updateTransportista + id,
                    // isArray:false,
                    header: {'Content-Type': 'application/json'}
                }
            });

        },
        deleteTransportista: function (id) {
            return $resource(api, {}, {
                ejecutar: {
                    method: 'DELETE',
                    url: dir.deleteTransportista + id
                }
            });
        }




    };

});

