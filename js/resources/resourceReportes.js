/**
 * Created by PROGRAMADOR on 13/07/2016.
 */

app.factory('resourceReportes', function ($resource) {
    return {

        getReporteHistorico: function (inicio, fin) {
            return $resource(api, {}, {
                ejecutar: {
                    url: api + '/reportes/getReporteHistoricoPagos/' + inicio + '/' + fin + '/',
                    method: "GET",
                    headers: {
                        'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }

            });
        }
    };
});
