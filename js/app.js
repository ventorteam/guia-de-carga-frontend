var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'ngMessages',
    'kendo.directives'
    // 'ngFileSaver'
]);

app.factory('LoadingInjector', function ($location) {
    var sessionInjector = {
        request: function (config) {
            $.blockUI({message: '<img src="img/cargando.gif">'});
            return config;
        },
        response: function (response) {
            $.unblockUI();
            return response;
        }
    };
    return sessionInjector;
});

app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('LoadingInjector');
    }]);