app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/dashboard', {
            templateUrl: 'template/dashboard.html',
        })
        .when('/duenoszona', {
            templateUrl: 'template/duenoszona.html',
            controller: 'duenoCtrl'
        })
        .when('/transportista', {
            templateUrl: 'template/transportistas.html',
            controller: 'transportistaCtrl'
        })
        .when('/zonas', {
            templateUrl: 'template/zonas.html',
            controller: 'zonasCtrl'
        })
        .when('/tipo_camion', {
            templateUrl: 'template/tipo_camion.html',
            controller: 'tipoCamionCtrl'
        })
        .when('/camion', {
            templateUrl: 'template/camion.html',
            controller: 'camionCtrl'
        })
        .when('/guias_Despachadas', {
            templateUrl: 'template/guias_despachadas.html',
            controller: 'guiasDespachadasCtrl'
        })
        .when('/reporteConteo', {
            templateUrl: 'template/reporteConteo.html',
            controller: 'reporteConteoCtrl'
        })
        .when('/guias_SinDespachar', {
            templateUrl: 'template/guias_index.html',
            controller: 'guiasCtrl'
        })
        .when('/costos', {
            templateUrl: 'template/costos.html',
            controller: 'costosCtrl'
        })
        .when('/tabulador', {
            templateUrl: 'template/tabulador.html',
            controller: 'tabuladorCtrl'
        })
        .when('/moduloMotivo', {
            templateUrl: 'template/moduloMotivo.html',
            controller: 'motivoCtrl'
        })
        .when('/pagoFleteros', {
            templateUrl: 'template/pagoFleteros.html',
            controller: 'reportePagoCtrl'
        })
        .when('/reporteInventario', {
            templateUrl: 'template/reporteInventario.html',
            controller: 'reporteInventarioCtrl'
        })
        .when('/reporteHistorico', {
            templateUrl: 'template/reporteHistorico.html',
            controller: 'reporteHistoricoCtrl'
        })
        .when('/reportes', {
            templateUrl: 'template/moduloReportes.html',
            controller: 'reportesCtrl'
        })
        .when('/empresa', {
            templateUrl: 'template/empresa.html',
            controller: 'empresaCtrl'
        })

        .otherwise({redirectTo: '/dashboard'});
}]);