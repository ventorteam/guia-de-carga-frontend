app.controller('guiasCtrl', function ($scope, $route, resourceGuias, resourceTabulador, resourceCiudades, resourceTransportista, $http, resourceCamion, resourceGuiasDespachadas, $timeout) {


    $('#start').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start = $("#start").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end = $("#end").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start.max(end.value());

    end.min(start.value());


    resourceGuias.getGuiasSinDespachar().ejecutar(function (data) {
        $scope.guiaSinDespachar = data.GuiasSinDespachar;
        console.log(data);
    }, function (error) {
        console.log(error);
    });

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.arrayFacturas = [];
    $scope.arrayClientes = [];
    $scope.arrayClientesLargo = [];
    $scope.PesoTotalFacturas = null;
    $scope.VolumenTotalFacturas = null;
    $scope.MontoTotalFacturas = null;


    $scope.seleccionarFactura = function ($index) {
        var PesoTotalFacturas;
        $scope.state = $scope.FacturasSinGuia[$index].status;
        console.log($scope.state);
        if ($scope.state) {

            //FACTURAS
            $scope.arrayFacturas.push($scope.FacturasSinGuia[$index].Numerofactura);
            console.log($scope.arrayFacturas[0]);

            //PESO           
            $scope.PesoTotalFacturas += parseFloat($scope.FacturasSinGuia[$index].PesoFactura);
            console.log($scope.PesoTotalFacturas);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);

            //VOLUMEN
            $scope.VolumenTotalFacturas += parseFloat($scope.FacturasSinGuia[$index].VolumentFactura);
            console.log($scope.VolumenTotalFacturas);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);

            //MONTO
            $scope.MontoTotalFacturas += parseFloat($scope.FacturasSinGuia[$index].MontoTotal);
            console.log($scope.MontoTotalFacturas);

            //NUMERO DE CLIENTES
            $scope.arrayClientesLargo.push($scope.FacturasSinGuia[$index].CodigoCliente);
            console.log($scope.arrayClientesLargo);
            var result = $scope.arrayClientes.indexOf($scope.FacturasSinGuia[$index].CodigoCliente);
            console.log(result)
            if (result >= 0) {
                console.log("no agregarlo");
                console.log($scope.arrayClientes)
            } else {
                $scope.arrayClientes.push($scope.FacturasSinGuia[$index].CodigoCliente);
                console.log("agregado");
                console.log($scope.arrayClientes);
            }

            console.log($scope.PesoTotalFacturas);
            console.log($scope.arrayFacturas.length);
        } else {
            $scope.posini = $scope.arrayFacturas.indexOf($scope.FacturasSinGuia[$index].Numerofactura);
            $scope.arrayFacturas.splice($scope.posini, 1);

            //PESO
            $scope.PesoTotalFacturas = parseFloat($scope.PesoTotalFacturas) - parseFloat($scope.FacturasSinGuia[$index].PesoFactura);
            console.log($scope.pesoFixed);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);

            //VOLUMEN
            $scope.VolumenTotalFacturas = parseFloat($scope.VolumenTotalFacturas) - parseFloat($scope.FacturasSinGuia[$index].VolumentFactura);
            console.log($scope.VolumenTfixed);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);

            //MONTO
            $scope.MontoTotalFacturas = parseFloat($scope.MontoTotalFacturas) - parseFloat($scope.FacturasSinGuia[$index].MontoTotal);
            console.log($scope.MontoTotalFacturas);


            //NUMERO DE CLIENTES
            var result = $scope.arrayClientesLargo.indexOf($scope.FacturasSinGuia[$index].CodigoCliente);
            $scope.arrayClientesLargo.splice(result, 1);
            result = $scope.arrayClientesLargo.indexOf($scope.FacturasSinGuia[$index].CodigoCliente);
            if (result === -1) {
                var borrar = $scope.arrayClientes.indexOf($scope.FacturasSinGuia[$index].CodigoCliente);
                $scope.arrayClientes.splice(borrar, 1);
            }
        }
    };

    $scope.RangoFechaFacturas = function () {
        var inicio = $("#start").val();
        var fin = $("#end").val();
        console.log(inicio);
        resourceGuias.getFacturasSinGuia(inicio, fin).ejecutar(function (data) {
            $scope.FacturasSinGuia = data.FacturaSinGuia;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    resourceTabulador.getEstadosTabulados().ejecutar(function (data) {
        $scope.estados = data.estado;
    }, function (error) {
        console.log(error);
    });

    $scope.ciudadesTab = function () {
        console.log('ESTADO ' + $scope.estadoObj);
        $scope.ObjEstado = JSON.parse($scope.estadoObj);

        resourceTabulador.getCiudadesTabuladas($scope.ObjEstado.CODESTA).ejecutar(function (data) {
            $scope.ciudades = data.ciudad;
        }, function (error) {
            console.log(error);
        });

    };


    resourceTransportista.getTransportistasCamion().ejecutar(function (data) {
        $scope.transportista = data.Transportistas;
        console.log('Transportistas ' + data);
    }, function (error) {
        console.log(error);
    });

    $scope.changeCamiones = function () {
        // var transportista = $('#transportista').val();
        console.log($scope.chofer);
        resourceGuias.getCamionesPorTransportista($scope.chofer).ejecutar(function (data) {
            $scope.camiones = data.camion;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.resetSelect = function () {
        if (angular.isDefined($scope.n_estado, $scope.cod_ciudad)) {
            delete $scope.n_estado;
            delete $scope.cod_ciudad;
        }
    };

    $scope.resetSelect2 = function () {
        if (angular.isDefined($scope.chofer, $scope.camionero)) {
            delete $scope.chofer;
            delete $scope.camionero;
        }
    };

    var idGuia = null;

    $scope.crearGuia = function () {
        // $scope.vaciarGuia();

        $scope.fecha = moment().utc().format('YYYY-MM-DD');

        console.log('ESTADO ' + $scope.estadoObj);
        $scope.ObjEstado = JSON.parse($scope.estadoObj);
        console.log($scope.ciudadObj);
        $scope.ObjCiudad = JSON.parse($scope.ciudadObj);


        $scope.datos = {
            "guia": {
                "fechacrea": $scope.fecha,
                "nfact": $scope.arrayFacturas.length,
                "nclie": $scope.arrayClientes.length,
                "pesott": $scope.pesoFixed,
                "voltt": $scope.VolumenTfixed,
                "codtransp": null,
                "id_camion": null,
                "codesta": $scope.ObjEstado.CODESTA,
                "codciud": $scope.ObjCiudad.CODCIUD,
                "fechadesp": null,
                "montott": $scope.MontoTotalFacturas,
                "nombre_ciudad": $scope.ObjCiudad.NOMBCIUD,
                "nombre_estado": $scope.ObjEstado.NOMBESTA.trim()
            }
        };

        $scope.datosjson = JSON.stringify($scope.datos);
        console.log($scope.datosjson);
        if ($scope.arrayFacturas.length !== 0) {

            resourceGuias.postGuia().ejecutar($scope.datosjson, function (data) {
                $scope.recargarFacturasSinGuia();
                console.log(data);


                // $timeout( function(){ $scope.imprimirGuiaSinD() }, 8000);


                //ULTIMO NUMERO DE GUIA
                resourceGuias.getGuiaID().ejecutar(function (data) {
                    UIkit.notify({
                        message: 'Guia creada con exito',
                        status: 'success',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                    // console.log(data);
                    idGuia = data.numero.AUTO_INCREMENT - 1;
                    // console.log("El id de la guia creada es: " + idGuia);

                    updatefactura.facturaEncabezado(idGuia);
                    updatefactura.facturaDetalle(idGuia);

                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });
        } else {
            // alert('Debe seleccionar al menos una factura');
            UIkit.notify({
                message: 'Debe seleccionar al menos una factura',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            //$scope.reloadRoute();
        }

    };

    var updatefactura = {
        facturaEncabezado: function (idGuia) {
            for (var i = 0, max = $scope.arrayFacturas.length; i < max; i++) {
                console.log(idGuia);
                $scope.datosE = {
                    "TFACHISA": {
                        "NUMEGUIA": idGuia
                    }
                };
                console.log($scope.arrayFacturas[i]);
                $scope.datosfacturaE = JSON.stringify($scope.datosE);

                resourceGuias.updateFacturaE($scope.arrayFacturas[i]).ejecutar($scope.datosfacturaE, function (success) {
                    //$scope.vaciarGuia();
                    //$scope.recargarGuia();
                    console.log(success);
                    console.log("BIEN EN ENCABEZADO FACTURA");
                }, function (error) {
                    console.log("ERROR EN ENCABEZADO FACTURA");
                    console.log(error);
                });

            }
        },
        facturaDetalle: function (idGuia) {
            for (var i = 0, max = $scope.arrayFacturas.length; i < max; i++) {

                $scope.datosD = {
                    "TFACHISB": {
                        "NUMEGUIA": idGuia
                    }
                };
                $scope.datosfacturaD = JSON.stringify($scope.datosD);
                resourceGuias.updateFacturaD($scope.arrayFacturas[i]).ejecutar($scope.datosfacturaD, function (success) {
                    //$scope.vaciarGuia();
                    //$scope.recargarGuia();
                    console.log(success);
                    console.log("BIEN EN DETALLE FACTURA");
                }, function (error) {
                    console.log("ERROR EN DETALLE FACTURA");
                    console.log(error);
                });

            }
        }
    };

    $scope.vaciarGuia = function () {

        $scope.arrayFacturas.length = 0;
        $scope.arrayClientes.length = 0;
        $scope.PesoTotalFacturas = 0;
        $scope.VolumenTotalFacturas = 0;
        $scope.MontoTotalFacturas = 0;

    };

    $scope.recargarGuias = function () {

        resourceGuias.getGuiasSinDespachar().ejecutar(function (data) {
            $scope.guiaSinDespachar = data.GuiasSinDespachar;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    //Metodo para despachar
    $scope.postDespachoGuia = function () {
        $scope.objCamion = JSON.parse($scope.camionObj);
        console.log($scope.objCamion);
        console.log($scope.objCamion.capacidad);

        $scope.fechadesp = moment().utc().format('YYYY-MM-DD');
        // debugger;

        $scope.datos = {
            "guia": {
                "fechacrea": $scope.FECHACREA,
                "nfact": $scope.NFACT,
                "nclie": $scope.NCLIE,
                "pesott": $scope.PESOTT,
                "voltt": $scope.VOLTT,
                "codtransp": $scope.chofer,
                "codesta": $scope.CODESTA,
                "codciud": $scope.CODCIUD,
                "fechadesp": $scope.fechadesp,
                "montott": $scope.MONTOTT,
                "id_flete": null,
                "id_camion": $scope.objCamion.placa,
                "nombre_ciudad": $scope.NOMBCIUDAD,
                "nombre_estado": null
            }
        };

        $scope.datosjson = JSON.stringify($scope.datos);
        console.log($scope.datosjson);
        console.log(parseFloat($scope.PESOTT).toFixed(2));
        console.log(parseFloat($scope.objCamion.capacidad).toFixed(2));

        // debugger;
        if (parseFloat($scope.PESOTT) <= parseFloat($scope.objCamion.capacidad)) {
            resourceGuias.updateGuia($scope.ID).ejecutar($scope.datosjson, function (data) {
                UIkit.notify({
                    message: 'Guia actualizada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.recargarFacturasSinGuia();
                console.log(data);
            }, function (error) {
                UIkit.notify({
                    message: 'Error al actualizar la guia',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });
        }
        else {
            //alert('Error. El peso de la guia supera la capacidad del camión!');
            UIkit.notify({
                message: 'Error. El peso de la guia supera la capacidad del camión!',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
        }
    };

    $scope.recargarGuiaMod = function () {
        $scope.FacturasTotales = 0;
        $scope.ClientesTotales = 0;
        $scope.PesoTotalFacturas = 0;
        $scope.VolumenTotalFacturas = 0;
        $scope.MontoTotalFacturas = 0;
        $scope.ArrayClienteViejo.length = 0;
        $scope.ArrayClienteNuevo.length = 0;

    };

    $scope.ArrayClienteViejo = [];
    $scope.ArrayClienteNuevo = [];

    $scope.visualizarSinDespachar = function ($index) {
        $scope.recargarGuiaMod();

        $scope.id = $scope.guiaSinDespachar[$index].ID;
        $scope.FECHACREA = $scope.guiaSinDespachar[$index].FECHACREA;
        $scope.NFACT = $scope.guiaSinDespachar[$index].NFACT;
        $scope.NCLIE = $scope.guiaSinDespachar[$index].NCLIE;
        $scope.MONTOTT = $scope.guiaSinDespachar[$index].MONTOTT;
        $scope.PESOTT = $scope.guiaSinDespachar[$index].PESOTT;
        $scope.VOLTT = $scope.guiaSinDespachar[$index].VOLTT;
        $scope.CODCIUDAD = $scope.guiaSinDespachar[$index].CODCIUD;
        $scope.NOMBRE_CIUDAD = $scope.guiaSinDespachar[$index].NOMBRE_CIUDAD;
        $scope.CODESTA = $scope.guiaSinDespachar[$index].CODESTA;
        console.log("NOMBRE CIUDAD: " + $scope.NOMBRE_CIUDAD);
        resourceGuiasDespachadas.getUniCiudad($scope.CODESTA, $scope.CODCIUDAD).ejecutar(function (success) {
            $scope.NOMBCIUDAD = success.ciudad.NOMBCIUD;
            console.log('Nombre de la ciudad ' + $scope.NOMBCIUDAD);
        }, function (error) {

        });
        resourceGuiasDespachadas.getCalculoFacturaGuia($scope.guiaSinDespachar[$index].ID).ejecutar(function (data) {
            $scope.facturaConGuiaE = data.Facturas;
            console.log($scope.facturaConGuiaE);
            console.log($scope.guiaSinDespachar[$index].ID);

            for (var i = 0; i < $scope.facturaConGuiaE.length; i++) {
                $scope.ArrayClienteViejo.push($scope.facturaConGuiaE[i].CodigoCliente);
                var result = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[i].CodigoCliente);
                if (result >= 0) {
                    console.log("No Agregar Cliente N");

                } else {
                    $scope.ArrayClienteNuevo.push($scope.facturaConGuiaE[i].CodigoCliente);
                    console.log("Agrrrgar Cliente N");
                }
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;
            console.log("CLIENTE VIEJO: " + $scope.ArrayClienteViejo);
            console.log("CLIENTE NUEVO: " + $scope.ArrayClienteNuevo);
        }, function (error) {
            console.log(error);
        });
        resourceGuiasDespachadas.getCalculoFacturaGuia($scope.guiaSinDespachar[$index].ID).ejecutar(function (data) {
            $scope.facturaConGuiaE = data.Facturas;
            console.log($scope.facturaConGuiaE);
            console.log($scope.guiaSinDespachar[$index].ID);
        }, function (error) {
            console.log(error);
        });
        resourceGuias.getReporteGuiaPortada($scope.guiaSinDespachar[$index].ID).ejecutar(function (data) {
            $scope.ReporteGuias = data.ReporteGuia;
            console.log(data);
        }, function (error) {
            console.log(error);
        });

        $http({
            method: "GET",
            url: dir.calculoCajaProductos + $scope.guiaSinDespachar[$index].ID
        }).then(function mySucces(response) {
            console.log(response.data.ReporteGuiaD);
            $scope.CalculoCajaProductos = response.data.ReporteGuiaD;

            $scope.imprimirGuiaSinD = function () {

                $.blockUI({message: '<img src="img/cargando.gif">'});

                $timeout(function () {
                    var columns = [
                        {title: "Tipo", dataKey: "tipo"},
                        {title: "Numero", dataKey: "numero"},
                        {title: "Cod. Cliente", dataKey: "codigoclie"},
                        {title: "Nombre Cliente", dataKey: "nombreclie"},
                        {title: "Cajas", dataKey: "cajas"}
                    ];
                    // Only pt supported (not mm or in)
                    var doc = new jsPDF('p', 'pt', 'letter');
                    doc.text(40, 40, 'DISTRIBUCIONES LA PRINCIPAL LA GRITA C.A.');
                    doc.setFontSize(9);
                    doc.text(440, 40, 'Fecha: ' + $scope.FECHACREA);
//                doc.text(500, 40 );
                    doc.text(440, 60, 'No. Guia: ' + $scope.id);
                    doc.text(440, 80, 'ZONA: ' + $scope.NOMBCIUDAD);
//                doc.text(500, 60);

                    doc.text(40, 80, 'Oficina Transporte:');
                    doc.text(140, 80, '0276-3919003');
                    doc.text(220, 80, '0414-0750569');
                    doc.text(40, 100, 'Oficina Adminstracion:');
                    doc.text(140, 100, '0276-3919003');
                    doc.setFontSize(8);
                    doc.text(40, 120, 'Total Factura:');
                    doc.text(100, 120, $scope.NFACT);
                    doc.text(140, 120, 'Total Clientes:');
                    doc.text(200, 120, $scope.NCLIE);
                    doc.text(240, 120, 'Total Peso:');
                    doc.text(290, 120, $scope.PESOTT);
                    doc.text(330, 120, 'Total Volumen:');
                    doc.text(400, 120, $scope.VOLTT);
                    doc.text(440, 120, 'Total Monto:');
                    doc.text(490, 120, $scope.MONTOTT);
                    doc.autoTable(columns, $scope.ReporteGuias, {
                        theme: 'striped',
                        tableWidth: 'auto',
                        styles: {
                            fontSize: 9,
                            font: 'Arial',
                            lineWidth: 0.5,
                            rowHeight: 20,
                            overflow: 'linebreak',
                            valign: 'middle',
                            halign: 'center'
                        },
                        columnStyles: {
                            id: {fillColor: 99},
                            lineWidth: 2,
                            rowHeight: 20,
                            fontSize: 15,
                            font: 'Arial',
                            valign: 'middle',
                            halign: 'center',
                            overflow: 'linebreak'
                        },
                        margin: {top: 140},
                        beforePageContent: function (data) {

                        }
                    });
                    doc.setFont('Arial');
                    doc.setFontSize(9);
                    doc.text(40, doc.autoTableEndPosY() + 40, 'Nombre del Chofer:   ___________________________');
                    doc.text(280, doc.autoTableEndPosY() + 40, 'Despachado:   ___________________________');
                    doc.text(40, doc.autoTableEndPosY() + 60, 'Camion y Placa:   ___________________________');
                    doc.text(280, doc.autoTableEndPosY() + 60, 'Entregado:   ___________________________');
                    doc.text(40, doc.autoTableEndPosY() + 80, 'Cedula:   ___________________________');
                    doc.text(280, doc.autoTableEndPosY() + 80, 'Recibido por:   ___________________________');
                    for (var i = 0; i < $scope.CalculoCajaProductos.length; i++) {
                        console.log($scope.CalculoCajaProductos);
                        doc.addPage();
                        doc.setFont('Arial');
                        doc.setFontSize(14);
                        doc.text(40, 60, 'No. Guia: ' + $scope.id);
                        doc.text(220, 60, 'Zona de despacho: ');
                        doc.text(220, 80, $scope.NOMBCIUDAD);
                        doc.setFontSize(8);
                        doc.text(460, 80, 'Dueno por zona: ' + $scope.CalculoCajaProductos[i].dueno + ' ' + $scope.CalculoCajaProductos[i].apellido);
                        doc.text(460, 65, $scope.CalculoCajaProductos[i].nombre);
                        doc.text(40, 120, 'Total Factura:');
                        doc.text(100, 120, $scope.NFACT);
                        doc.text(140, 120, 'Total Clientes:');
                        doc.text(200, 120, $scope.NCLIE);
                        doc.text(240, 120, 'Total Peso:');
                        doc.text(290, 120, $scope.PESOTT);
                        doc.text(330, 120, 'Total Volumen:');
                        doc.text(400, 120, $scope.VOLTT);
                        var columnas = [
                            {title: 'Codigo', dataKey: 'codiProducto'},
                            {title: 'Ubic', dataKey: 'Ubic'},
                            {title: 'Descripcion', dataKey: 'desc'},
                            {title: 'Cajas', dataKey: 'cajas'},
                            {title: '3/4', dataKey: 'trescuarto'},
                            {title: '1/2', dataKey: 'mediacaja'},
                            {title: '1/4', dataKey: 'cuartocaja'},
                            {title: 'UNID', dataKey: 'unid'}
                        ];
                        doc.autoTable(columnas, $scope.CalculoCajaProductos[i].productos, {
                            theme: 'striped',
                            tableWidth: 'auto',
                            styles: {
                                fontSize: 9,
                                font: 'Arial',
                                lineWidth: 0.5,
                                rowHeight: 20,
                                overflow: 'linebreak',
                                valign: 'middle',
                                halign: 'center'
                            },
                            columnStyles: {
                                id: {fillColor: 99},
                                lineWidth: 2,
                                rowHeight: 20,
                                fontSize: 15,
                                font: 'Arial',
                                valign: 'middle',
                                halign: 'center',
                                overflow: 'linebreak'
                            },
                            margin: {top: 140},
                            beforePageContent: function (data) {
                            }
                        });
                        $scope.acumCajas = 0;
                        $scope.acumtrescuarto = 0;
                        $scope.acummediacaja = 0;
                        $scope.acumcuartocaja = 0;
                        $scope.acumunid = 0;
                        for (var y = 0, max = $scope.CalculoCajaProductos[i].productos.length; y < max; y++) {
                            $scope.acumCajas += $scope.CalculoCajaProductos[i].productos[y].cajas;
                            $scope.acumtrescuarto += $scope.CalculoCajaProductos[i].productos[y].trescuarto;
                            $scope.acummediacaja += $scope.CalculoCajaProductos[i].productos[y].mediacaja;
                            $scope.acumcuartocaja += $scope.CalculoCajaProductos[i].productos[y].cuartocaja;
                            $scope.acumunid += $scope.CalculoCajaProductos[i].productos[y].unid;
                        }
                        doc.setFont('Arial');
                        doc.setFontSize('9');
                        doc.text(400, doc.autoTableEndPosY() + 15, 'Total CAJAS = ' + $scope.acumCajas);
                        doc.text(400, doc.autoTableEndPosY() + 25, 'Total 3/4 = ' + $scope.acumtrescuarto);
                        doc.text(400, doc.autoTableEndPosY() + 35, 'Total 1/2 = ' + $scope.acummediacaja);
                        doc.text(400, doc.autoTableEndPosY() + 45, 'Total 1/4 = ' + $scope.acumcuartocaja);
                        doc.text(400, doc.autoTableEndPosY() + 55, 'Total UNIDAD = ' + $scope.acumunid);
                    }
                    doc.save('reporteGuiaSinDespachar.pdf');
                    $.unblockUI();
                }, 4000);
            };
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });


        $scope.guardar = function ($index) {

            $scope.datosE = {
                "TFACHISA": {
                    "NUMEGUIA": ""
                }
            };
            $scope.datosD = {
                "TFACHISB": {
                    "NUMEGUIA": ""
                }
            };

            $scope.arrFinal = [];
            for (var i = 0, max = $scope.ArrayFacturaDelete.length; i < max; i++) {
                $scope.arrFinal.push($scope.ArrayFacturaDelete[i]);
            }
            resourceGuias.deleteFacturaDeGuiaA(JSON.stringify($scope.arrFinal)).ejecutar($scope.datosE, function (success) {
                resourceGuias.deleteFacturaDeGuiaB(JSON.stringify($scope.arrFinal)).ejecutar($scope.datosD, function (success) {
                    console.log(success);
                    console.log($scope.arrFinal);
                    $scope.recargarGuias();
                }, function (error) {
                    console.log(error);
                });
                console.log(success);
            }, function (error) {
                console.log(error);
            });

            $scope.datosNuevaGuia = {
                "guia": {
                    "nfact": $scope.FacturasTotales,
                    "nclie": $scope.ClientesTotales,
                    "pesott": $scope.PesoTotalFacturas,
                    "voltt": $scope.VolumenTotalFacturas,
                    "montott": $scope.MontoTotalFacturas
                }
            };

            $scope.datosJSON = JSON.stringify($scope.datosNuevaGuia);
            console.log("GUIA NUEVA MODIFICADA: " + $scope.datosJSON);

            resourceGuias.updateGuiaMod($scope.id).ejecutar($scope.datosJSON, function (success) {
                UIkit.notify({
                    message: 'Guia actualizada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log($scope.datosNuevaGuia);
            }, function (error) {
                console.log(error);
            });
        };
        console.log($scope.id);

        $scope.PesoTotalFacturas = $scope.PESOTT;
        $scope.VolumenTotalFacturas = $scope.VOLTT;
        $scope.MontoTotalFacturas = $scope.MONTOTT;
        $scope.FacturasTotales = $scope.NFACT;
        $scope.ArrayClienteViejo = [];

    };

    $scope.despacharGuia = function ($index) {

        $scope.APELLIDO = $scope.guiaSinDespachar[$index].APELLIDO;
        $scope.CODCIUD = $scope.guiaSinDespachar[$index].CODCIUD;
        $scope.CODESTA = $scope.guiaSinDespachar[$index].CODESTA;
        $scope.CODTRANSP = $scope.guiaSinDespachar[$index].CODTRANSP;
        $scope.FECHACREA = $scope.guiaSinDespachar[$index].FECHACREA;
        $scope.FECHADESP = $scope.guiaSinDespachar[$index].FECHADESP;
        $scope.ID = $scope.guiaSinDespachar[$index].ID;
        $scope.MONTOTT = $scope.guiaSinDespachar[$index].MONTOTT;
        $scope.NCLIE = $scope.guiaSinDespachar[$index].NCLIE;
        $scope.NFACT = $scope.guiaSinDespachar[$index].NFACT;
        $scope.NOMBRE = $scope.guiaSinDespachar[$index].NOMBRE;
        $scope.NOMBRE_CIUDAD = $scope.guiaSinDespachar[$index].NOMBRE_CIUDAD;
        $scope.PESOTT = $scope.guiaSinDespachar[$index].PESOTT;
        $scope.VOLTT = $scope.guiaSinDespachar[$index].VOLTT;
        resourceGuiasDespachadas.getUniCiudad($scope.CODESTA, $scope.CODCIUDAD).ejecutar(function (success) {
            $scope.NOMBCIUDAD = success.ciudad.NOMBCIUD;
            console.log('Nombre de la ciudad ' + $scope.NOMBCIUDAD);
        }, function (error) {

        });

        console.log($scope.ID);
        resourceGuiasDespachadas.getCalculoFacturaGuia($scope.ID).ejecutar(function (data) {
            $scope.factura = data.Facturas;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
        resourceGuias.getReporteGuiaPortada($scope.ID).ejecutar(function (data) {
            $scope.ReporteGuias = data.ReporteGuia;
            console.log(data);
        }, function (error) {
            console.log(error);
        });

        resourceGuias.getCalculoCajaProductos($scope.ID).ejecutar(function (data) {
            $scope.ReporteGuiasD = data.ReporteGuiaD;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.recargarFacturasSinGuia = function () {
        resourceGuias.getGuiasSinDespachar().ejecutar(function (data) {
            $scope.guiaSinDespachar = data.GuiasSinDespachar;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.deleteGuia = function ($index) {

        $scope.datosE = {
            "TFACHISA": {
                "NUMEGUIA": ""
            }
        };
        $scope.datosD = {
            "TFACHISB": {
                "NUMEGUIA": ""
            }
        };

        $scope.guiaupE = JSON.stringify($scope.datosE);
        $scope.guiaupD = JSON.stringify($scope.datosD);
        UIkit.modal.confirm('Estas seguro(a)?', function () {
            resourceGuiasDespachadas.getFacturaConGuiaE($scope.guiaSinDespachar[$index].ID).ejecutar(function (success) {
                console.log(success.Encabezado);
                for (var i = 0, max = success.Encabezado.length; i < max; i++) {

                    //update a Encabezado 
                    resourceGuias.updateFacturasinGuiaA($scope.guiaSinDespachar[$index].ID).ejecutar($scope.guiaupE, function (success) {

                        console.log(success);
                        resourceGuias.updateFacturasinGuiaD($scope.guiaSinDespachar[$index].ID).ejecutar($scope.guiaupD, function (success) {
                            console.log(success);
                        }, function (error) {
                            console.log(error);
                        });

                    }, function (error) {
                        console.log(error);
                    });
                }
                resourceGuias.deleteGuia($scope.guiaSinDespachar[$index].ID).ejecutar(function (success) {
                    UIkit.notify({
                        message: 'Guia eliminada con exito.',
                        status: 'success',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                    console.log(success);
                    $scope.recargarFacturasSinGuia();
                }, function (error) {
                    console.log(error);
                });
            }, function (error) {

            });
            UIkit.modal.alert('Eliminado!');
        });
    };

//modificar guia con facturas sin despachar
    $scope.ArrayFacturaDelete = [];

    $scope.seleccionarFacturaConGuia = function ($index) {

        $scope.state = $scope.facturaConGuiaE[$index].status;
        console.log($scope.state);

        if ($scope.state) {

            //FACTURAS
            $scope.FacturasTotales = $scope.FacturasTotales - 1;

            $scope.ArrayFacturaDelete.push($scope.facturaConGuiaE[$index].Numerofactura);

            //NUMERO DE CLIENTES
            var result = $scope.ArrayClienteViejo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            $scope.ArrayClienteViejo.splice(result, 1);
            result = $scope.ArrayClienteViejo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            if (result === -1) {
                var borrar = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
                $scope.ArrayClienteNuevo.splice(borrar, 1);
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;
            //PESO
            $scope.PesoTotalFacturas = parseFloat($scope.PesoTotalFacturas) - parseFloat($scope.facturaConGuiaE[$index].PesoFactura);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);
            console.log($scope.pesoFixed);

            //VOLUMEN
            $scope.VolumenTotalFacturas = parseFloat($scope.VolumenTotalFacturas) - parseFloat($scope.facturaConGuiaE[$index].VolumentFactura);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);
            console.log($scope.VolumenTfixed);

            //MONTO TOTAL
            $scope.MontoTotalFacturas = $scope.MontoTotalFacturas - $scope.facturaConGuiaE[$index].MontoTotal;
            console.log($scope.MontoTotalFacturas);

        } else {
            //OPERACIONES

            //FACTURAS TOTALES
            $scope.FacturasTotales = $scope.FacturasTotales + 1;

            var posini = $scope.ArrayFacturaDelete.indexOf($scope.facturaConGuiaE[$index].Numerofactura);
            $scope.ArrayFacturaDelete.splice(posini, 1);

            //CLIENTES
            $scope.ArrayClienteViejo.push($scope.facturaConGuiaE[$index].CodigoCliente);
            console.log($scope.ArrayClienteViejo);
            var result = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            console.log(result)
            if (result >= 0) {
                console.log("no agregarlo");
                console.log($scope.ArrayClienteNuevo)
            } else {
                $scope.ArrayClienteNuevo.push($scope.facturaConGuiaE[$index].CodigoCliente);
                console.log("agregado");
                console.log($scope.ArrayClienteNuevo);
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;

            //PESO
            $scope.PesoTotalFacturas = parseFloat($scope.PesoTotalFacturas) + parseFloat($scope.facturaConGuiaE[$index].PesoFactura);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);
            console.log($scope.pesoFixed);

            //VOLUMEN
            $scope.VolumenTotalFacturas = parseFloat($scope.VolumenTotalFacturas) + parseFloat($scope.facturaConGuiaE[$index].VolumentFactura);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);
            console.log($scope.VolumenTfixed);

            //MONTO TOTAL
            $scope.MontoTotalFacturas = $scope.MontoTotalFacturas + $scope.facturaConGuiaE[$index].MontoTotal;
            console.log("Monto Total al destildar: " + $scope.MontoTotalFacturas);
        }
    };


});