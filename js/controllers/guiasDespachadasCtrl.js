app.controller('guiasDespachadasCtrl', function ($scope, dateFilter, resourceMotivo, resourceGuiasDespachadas, resourceGuias, $http, resourceTransportista, $timeout, resourceCamion, resourceLogin) {

    resourceGuiasDespachadas.getGuiasDespachadas().ejecutar(function (data) {
        $scope.guiaDespachada = data.GuiasDespachadas;
        console.log(data);
    }, function (error) {
        console.log(error);
    });

    $scope.recargarGuias = function () {
        resourceGuiasDespachadas.getGuiasDespachadas().ejecutar(function (data) {
            $scope.guiaDespachada = data.GuiasDespachadas;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    //cargar motivos
    resourceMotivo.getMotivo().ejecutar(function (data) {
        $scope.motivos = data.Motivos;
        console.log($scope.motivos);
    }, function (error) {
        console.log(error);
    });

    //Captura el id de la guia a entregar
    $scope.entregarGuia1 = function ($index) {
        $scope.id = $scope.guiaDespachada[$index].ID;

        //datepicker start
        $('#fentrega').click(function () {
            $('span[aria-controls="start_dateview"]').trigger("click");
        });

        var today = kendo.toString(kendo.parseDate(new Date()), "yyyy-MM-dd");
        console.log(today);

        var fechentre = $("#fentrega").kendoDateTimePicker({
            value: today,
            //change: startChange,
            format: "yyyy-MM-dd"
        }).data("kendoDateTimePicker");

        var fechentre = $("#fentrega").val();
        //datepicker end

        //funcion para asignar motivo y fecha de entrega a la guia
        $scope.entregarGuia = function () {
            $scope.MotivoFecha = {
                "guia": {
                    "fechaentrega": fechentre,
                    "motivo": $scope.id_motivo
                }
            };
            $scope.JSONmotivoFecha = JSON.stringify($scope.MotivoFecha);
            resourceGuiasDespachadas.updateGuiaMotivo($scope.id).ejecutar($scope.JSONmotivoFecha, function (success) {
                UIkit.notify({
                    message: 'Guia entregada.',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log($scope.JSONmotivoFecha);
                console.log(success);
                $scope.recargarGuias();
            }, function (error) {
                console.log(error);
            });
        };

    };

    $scope.FacturasConGuia = function ($index) {

        $scope.id = $scope.guiaDespachada[$index].ID;
        $scope.FECHACREA = $scope.guiaDespachada[$index].FECHACREA;
        $scope.NFACT = $scope.guiaDespachada[$index].NFACT;
        $scope.NCLIE = $scope.guiaDespachada[$index].NCLIE;
        $scope.PESOTT = $scope.guiaDespachada[$index].PESOTT;
        $scope.VOLTT = $scope.guiaDespachada[$index].VOLTT;
        $scope.CODCIUDAD = $scope.guiaDespachada[$index].CODCIUD;
        $scope.CODESTA = $scope.guiaDespachada[$index].CODESTA;
        $scope.MONTOTT = $scope.guiaDespachada[$index].MONTOTT;
        $scope.NOMBRE = $scope.guiaDespachada[$index].NOMBRE;
        $scope.APELLIDO = $scope.guiaDespachada[$index].APELLIDO;
        $scope.ID_CAMION = $scope.guiaDespachada[$index].ID_CAMION;
        $scope.CODTRANSP = $scope.guiaDespachada[$index].CODTRANSP;
        $scope.RIF = $scope.guiaDespachada[$index].RIF;
        $scope.NOMBEMPRESA = $scope.guiaDespachada[$index].NOMBEMPRESA;
        $scope.DESCRIPCION = $scope.guiaDespachada[$index].DESCRIPCION;
        resourceGuiasDespachadas.getUniCiudad($scope.CODESTA, $scope.CODCIUDAD).ejecutar(function (success) {
            $scope.NOMBCIUDAD = success.ciudad.NOMBCIUD;
            console.log('Nombre de la ciudad ' + $scope.NOMBCIUDAD);
        }, function (error) {

        });
        resourceGuiasDespachadas.getCalculoFacturaGuia($scope.guiaDespachada[$index].ID).ejecutar(function (data) {
            $scope.facturaConGuiaE = data.Facturas;
            console.log($scope.facturaConGuiaE);
            console.log($scope.guiaDespachada[$index].ID);
        }, function (error) {
            console.log(error);
        });
        console.log($scope.guiaDespachada[$index].ID);
        resourceGuias.getReporteGuiaPortada($scope.guiaDespachada[$index].ID).ejecutar(function (data) {
            $scope.ReporteGuias = data.ReporteGuia;
            $http({
                method: "GET",
                url: dir.calculoCajaProductos + $scope.guiaDespachada[$index].ID
            }).then(function mySucces(response) {
                console.log(response.data.ReporteGuiaD);
                $scope.CalculoCajaProductos = response.data.ReporteGuiaD;

                $scope.imprimir = function ($index) {

//        resourceGuias.getCalculoCajaProductos($scope.guiaDespachada[$index].ID).ejecutar(function (success) {
//            console.log(success);
//            $scope.CalculoCajaProductos = success.ReporteGuia;
//            console.log("este/n" + success.ReporteGuia);
//        }, function (error) {
//
//        });
                    $.blockUI({message: '<img src="img/cargando.gif">'});

                    $timeout(function () {
                        var columns = [
                            {title: "Tipo", dataKey: "tipo"},
                            {title: "Numero", dataKey: "numero"},
                            {title: "Cod. Cliente", dataKey: "codigoclie"},
                            {title: "Nombre Cliente", dataKey: "nombreclie"},
                            {title: "Cajas", dataKey: "cajas"}
                        ];
                        // Only pt supported (not mm or in)
                        var doc = new jsPDF('p', 'pt', 'letter');
                        doc.text(40, 40, 'DISTRIBUCIONES LA PRINCIPAL LA GRITA C.A.');
                        doc.setFontSize(9);
                        doc.text(440, 40, 'Fecha: ' + $scope.FECHACREA);
//                doc.text(500, 40 );
                        doc.text(440, 60, 'No. Guia: ' + $scope.id);
                        doc.text(440, 80, 'ZONA: ' + $scope.NOMBCIUDAD);
//                doc.text(500, 60);

                        doc.text(40, 80, 'Oficina Transporte:');
                        doc.text(140, 80, '0276-3919003');
                        doc.text(220, 80, '0414-0750569');
                        doc.text(40, 100, 'Oficina Adminstracion:');
                        doc.text(140, 100, '0276-3919003');
                        doc.setFontSize(8);
                        doc.text(40, 120, 'Total Factura:');
                        doc.text(100, 120, $scope.NFACT);
                        doc.text(140, 120, 'Total Clientes:');
                        doc.text(200, 120, $scope.NCLIE);
                        doc.text(240, 120, 'Total Peso:');
                        doc.text(290, 120, $scope.PESOTT);
                        doc.text(330, 120, 'Total Volumen:');
                        doc.text(399, 120, $scope.VOLTT);
                        doc.text(440, 120, 'Total Monto Bs:');
                        doc.text(510, 120, $scope.MONTOTT);
                        doc.autoTable(columns, $scope.ReporteGuias, {
                            theme: 'striped',
                            tableWidth: 'auto',
                            styles: {
                                fontSize: 9,
                                font: 'Arial',
                                lineWidth: 0.5,
                                rowHeight: 20,
                                overflow: 'linebreak',
                                valign: 'middle',
                                halign: 'center'
                            },
                            columnStyles: {
                                id: {fillColor: 99},
                                lineWidth: 2,
                                rowHeight: 20,
                                fontSize: 15,
                                font: 'Arial',
                                valign: 'middle',
                                halign: 'center',
                                overflow: 'linebreak'
                            },
                            margin: {top: 140},
                            beforePageContent: function (data) {

                            }
                        });
                        doc.setFont('Arial');
                        doc.setFontSize(9);
                        doc.text(40, doc.autoTableEndPosY() + 40, 'Nombre del Chofer: ' + $scope.NOMBRE + ' ' + $scope.APELLIDO);
                        doc.text(280, doc.autoTableEndPosY() + 40, 'Despachado:   ___________________________');
                        doc.text(40, doc.autoTableEndPosY() + 60, 'Camion y Placa: ' + $scope.DESCRIPCION + ' ' + $scope.ID_CAMION);
                        doc.text(280, doc.autoTableEndPosY() + 60, 'Entregado:   ___________________________');
                        doc.text(40, doc.autoTableEndPosY() + 80, 'Cedula: ' + $scope.CODTRANSP);
                        doc.text(40, doc.autoTableEndPosY() + 100, 'Empresa de Transporte: ' + $scope.RIF + ' ' + $scope.NOMBEMPRESA);
                        doc.text(280, doc.autoTableEndPosY() + 80, 'Recibido por:   ___________________________');
                        for (var i = 0; i < $scope.CalculoCajaProductos.length; i++) {
                            console.log($scope.CalculoCajaProductos);
                            doc.addPage();
                            doc.setFont('Arial');
                            doc.setFontSize(14);
                            doc.text(40, 60, 'No. Guia: ' + $scope.id);
                            doc.text(220, 60, 'Zona de despacho: ');
                            doc.text(220, 80, $scope.NOMBCIUDAD);
                            doc.setFontSize(8);
                            doc.text(460, 80, 'Dueno por zona: ' + $scope.CalculoCajaProductos[i].dueno + ' ' + $scope.CalculoCajaProductos[i].apellido);
                            doc.text(460, 65, $scope.CalculoCajaProductos[i].nombre);
                            doc.text(40, 120, 'Total Factura:');
                            doc.text(100, 120, $scope.NFACT);
                            doc.text(140, 120, 'Total Clientes:');
                            doc.text(200, 120, $scope.NCLIE);
                            doc.text(240, 120, 'Total Peso:');
                            doc.text(290, 120, $scope.PESOTT);
                            doc.text(330, 120, 'Total Volumen:');
                            doc.text(399, 120, $scope.VOLTT);
                            var columnas = [
                                {title: 'Codigo', dataKey: 'codiProducto'},
                                {title: 'Ubic', dataKey: 'Ubic'},
                                {title: 'Descripcion', dataKey: 'desc'},
                                {title: 'Cajas', dataKey: 'cajas'},
                                {title: '3/4', dataKey: 'trescuarto'},
                                {title: '1/2', dataKey: 'mediacaja'},
                                {title: '1/4', dataKey: 'cuartocaja'},
                                {title: 'UNID', dataKey: 'unid'}
                            ];
                            doc.autoTable(columnas, $scope.CalculoCajaProductos[i].productos, {
                                theme: 'striped',
                                tableWidth: 'auto',
                                styles: {
                                    fontSize: 9,
                                    font: 'Arial',
                                    lineWidth: 0.5,
                                    rowHeight: 20,
                                    overflow: 'linebreak',
                                    valign: 'middle',
                                    halign: 'center'
                                },
                                columnStyles: {
                                    id: {fillColor: 99},
                                    lineWidth: 2,
                                    rowHeight: 20,
                                    fontSize: 15,
                                    font: 'Arial',
                                    valign: 'middle',
                                    halign: 'center',
                                    overflow: 'linebreak'
                                },
                                margin: {top: 140},
                                beforePageContent: function (data) {
                                }
                            });
                            $scope.acumCajas = 0;
                            $scope.acumtrescuarto = 0;
                            $scope.acummediacaja = 0;
                            $scope.acumcuartocaja = 0;
                            $scope.acumunid = 0;
                            for (var y = 0, max = $scope.CalculoCajaProductos[i].productos.length; y < max; y++) {
                                $scope.acumCajas += $scope.CalculoCajaProductos[i].productos[y].cajas;
                                $scope.acumtrescuarto += $scope.CalculoCajaProductos[i].productos[y].trescuarto;
                                $scope.acummediacaja += $scope.CalculoCajaProductos[i].productos[y].mediacaja;
                                $scope.acumcuartocaja += $scope.CalculoCajaProductos[i].productos[y].cuartocaja;
                                $scope.acumunid += $scope.CalculoCajaProductos[i].productos[y].unid;
                            }
                            doc.setFont('Arial');
                            doc.setFontSize('9');
                            doc.text(400, doc.autoTableEndPosY() + 15, 'Total CAJAS = ' + $scope.acumCajas);
                            doc.text(400, doc.autoTableEndPosY() + 25, 'Total 3/4 = ' + $scope.acumtrescuarto);
                            doc.text(400, doc.autoTableEndPosY() + 35, 'Total 1/2 = ' + $scope.acummediacaja);
                            doc.text(400, doc.autoTableEndPosY() + 45, 'Total 1/4 = ' + $scope.acumcuartocaja);
                            doc.text(400, doc.autoTableEndPosY() + 55, 'Total UNIDAD = ' + $scope.acumunid);
                        }
                        doc.save('reporteGuiaDespachada.pdf');
                        $.unblockUI();

                    }, 4000);

                };
            }, function myError(response) {
                $scope.myWelcome = response.statusText;
            });
            console.log(data);
        }, function (error) {
            console.log(error);
        });

    };

    //modificar guia con facturas despachadas
    $scope.ArrayClienteViejo = [];
    $scope.ArrayClienteNuevo = [];

    $scope.changeCamiones = function () {
        var transportista = $('#transportista').val();
        console.log(transportista);
        resourceGuias.getCamionesPorTransportista(transportista).ejecutar(function (data) {
            $scope.camiones = data.camion;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.recargarGuiaModD = function () {
        $scope.FacturasTotales = 0;
        $scope.ClientesTotales = 0;
        $scope.PesoTotalFacturas = 0;
        $scope.VolumenTotalFacturas = 0;
        $scope.MontoTotalFacturas = 0;
        $scope.ArrayClienteViejo.length = 0;
        $scope.ArrayClienteNuevo.length = 0;

    };

    $scope.vaciarLogin = function () {

        $scope.user = "";
        $scope.pass = "";

    };

    //modificar transportista en guia despachada
    $scope.modificarGuiaT = function ($index) {
        $scope.vaciarLogin();
        $scope.recargarGuiaModD();

        $scope.id = $scope.guiaDespachada[$index].ID;
        $scope.FECHACREA = $scope.guiaDespachada[$index].FECHACREA;
        $scope.NFACT = $scope.guiaDespachada[$index].NFACT;
        $scope.NCLIE = $scope.guiaDespachada[$index].NCLIE;
        $scope.PESOTT = $scope.guiaDespachada[$index].PESOTT;
        $scope.VOLTT = $scope.guiaDespachada[$index].VOLTT;
        $scope.MONTOTT = $scope.guiaDespachada[$index].MONTOTT;
        $scope.CODCIUDAD = $scope.guiaDespachada[$index].CODCIUD;
        $scope.CODESTA = $scope.guiaDespachada[$index].CODESTA;
        $scope.NOMBRE_CIUDAD = $scope.guiaDespachada[$index].NOMBRE_CIUDAD;
        resourceGuiasDespachadas.getUniCiudad($scope.CODESTA, $scope.CODCIUDAD).ejecutar(function (success) {
            $scope.NOMBCIUDAD = success.ciudad.NOMBCIUD;
            console.log('Nombre de la ciudad ' + $scope.NOMBCIUDAD);
        }, function (error) {

        });
        resourceGuiasDespachadas.getCalculoFacturaGuia($scope.guiaDespachada[$index].ID).ejecutar(function (data) {
            $scope.facturaConGuiaE = data.Facturas;
            console.log($scope.facturaConGuiaE);
            console.log($scope.guiaDespachada[$index].ID);
            for (var i = 0; i < $scope.facturaConGuiaE.length; i++) {
                $scope.ArrayClienteViejo.push($scope.facturaConGuiaE[i].CodigoCliente);
                var result = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[i].CodigoCliente);
                if (result >= 0) {
                    console.log("No Agregar Cliente N");
                } else {
                    $scope.ArrayClienteNuevo.push($scope.facturaConGuiaE[i].CodigoCliente);
                    console.log("Agrrrgar Cliente N");
                }
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;
            console.log("CLIENTE VIEJO: " + $scope.ArrayClienteViejo);
            console.log("CLIENTE NUEVO: " + $scope.ArrayClienteNuevo);
        }, function (error) {
            console.log(error);
        });


        $scope.login = function ($index) {

            $scope.auth = $scope.user + ":" + $scope.pass;

            resourceLogin.getAuthUser($scope.auth).ejecutar(function (success) {
                console.log(success);
                console.log(success.response);
                $scope.executeLogin = success.response;

                if ($scope.executeLogin === 1) {

                    $scope.datosE = {
                        "TFACHISA": {
                            "NUMEGUIA": ""
                        }
                    };
                    $scope.datosD = {
                        "TFACHISB": {
                            "NUMEGUIA": ""
                        }
                    };

                    $scope.guiaupE = JSON.stringify($scope.datosE);
                    $scope.guiaupD = JSON.stringify($scope.datosD);

                    resourceGuiasDespachadas.getFacturaConGuiaE($scope.id).ejecutar(function (success) {
                        console.log(success.Encabezado);
                        for (var i = 0, max = success.Encabezado.length; i < max; i++) {

                            //update a Encabezado 
                            resourceGuias.updateFacturasinGuiaA($scope.id).ejecutar($scope.guiaupE, function (success) {
                                console.log(success);
                                resourceGuias.updateFacturasinGuiaD($scope.id).ejecutar($scope.guiaupD, function (success) {
                                    console.log(success);
                                }, function (error) {
                                    console.log(error);
                                });

                            }, function (error) {
                                console.log(error);
                            });
                        }
                        resourceGuias.deleteGuia($scope.id).ejecutar(function (success) {
                            //console.log(success);
                           // console.log("NUMERO GUIA A ELIMINAR: " + $scope.id);
                            UIkit.notify({
                                message: 'Guia eliminada',
                                status: 'success',
                                timeout: 3000,
                                pos: 'top-center'
                            });
                            $scope.recargarGuias();
                        }, function (error) {
                            UIkit.notify({
                                message: 'Error al eliminar guia',
                                status: 'danger',
                                timeout: 3000,
                                pos: 'top-center'
                            });
                            console.log(error);
                        });
                    }, function (error) {

                    });
                } else {
                   // UIkit.modal.alert('Usuario no autorizado');
                    UIkit.notify({
                        message: 'Usuario no autorizado',
                        status: 'warning',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                }
            }, function (error) {
                UIkit.notify({
                    message: 'Error al verificar credenciales',
                    status: 'warning',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });

        };

        $scope.deleteGuia = function ($index) {
            $scope.datosE = {
                "TFACHISA": {
                    "NUMEGUIA": ""
                }
            };
            $scope.datosD = {
                "TFACHISB": {
                    "NUMEGUIA": ""
                }
            };
            $scope.guiaupE = JSON.stringify($scope.datosE);
            $scope.guiaupD = JSON.stringify($scope.datosD);
            UIkit.modal.confirm('Estas seguro(a)?', function () {
                resourceGuiasDespachadas.getFacturaConGuiaE($scope.id).ejecutar(function (success) {
                    console.log(success.Encabezado);
                    for (var i = 0, max = success.Encabezado.length; i < max; i++) {

                        //update a Encabezado 
                        resourceGuias.updateFacturasinGuiaA($scope.id).ejecutar($scope.guiaupE, function (success) {
                            console.log(success);
                            resourceGuias.updateFacturasinGuiaD($scope.id).ejecutar($scope.guiaupD, function (success) {
                                console.log(success);
                            }, function (error) {
                                console.log(error);
                            });

                        }, function (error) {
                            console.log(error);
                        });
                    }
                    resourceGuias.deleteGuia($scope.id).ejecutar(function (success) {
                        console.log(success);
                        console.log("NUMERO GUIA A ELIMINAR: " + $scope.id);
                        $scope.recargarGuias();
                    }, function (error) {
                        console.log(error);
                    });
                }, function (error) {

                });
                UIkit.modal.alert('Eliminado!');
            });
        };

        $scope.guardarGuiaMod = function ($index) {

            $scope.datosE = {
                "TFACHISA": {
                    "NUMEGUIA": ""
                }
            };
            $scope.datosD = {
                "TFACHISB": {
                    "NUMEGUIA": ""
                }
            };
            $scope.arrFinal = [];
            for (var i = 0, max = $scope.ArrayFacturaDelete.length; i < max; i++) {
                $scope.arrFinal.push($scope.ArrayFacturaDelete[i]);
            }
            resourceGuias.deleteFacturaDeGuiaA(JSON.stringify($scope.arrFinal)).ejecutar($scope.datosE, function (success) {
                resourceGuias.deleteFacturaDeGuiaB(JSON.stringify($scope.arrFinal)).ejecutar($scope.datosD, function (success) {
                    console.log(success);
                    console.log($scope.arrFinal);
                }, function (error) {
                    console.log(error);
                });
                console.log(success);
            }, function (error) {
                console.log(error);
            });
            var transportista = $("#transportista").val();
            var camion = $("#camion").val();

            $scope.datosNuevaGuia = {
                "guia": {
                    "nfact": $scope.FacturasTotales,
                    "nclie": $scope.ClientesTotales,
                    "pesott": $scope.PesoTotalFacturas,
                    "voltt": $scope.VolumenTotalFacturas,
                    "montott": $scope.MontoTotalFacturas,
                    "codtransp": transportista,
                    "id_camion": camion
                }
            };

            console.log("NOMBRE TRANSPORTISTA: " + transportista);
            console.log("NOMBRE CAMION: " + camion);

            $scope.datosJSON = JSON.stringify($scope.datosNuevaGuia);
            console.log("GUIA NUEVA MODIFICADA: " + $scope.datosJSON);
            resourceGuiasDespachadas.updateGuiaModT($scope.id).ejecutar($scope.datosJSON, function (success) {
                UIkit.notify({
                    message: 'Guia actualizada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log($scope.datosNuevaGuia);
                $scope.recargarGuias();
            }, function (error) {
                console.log(error);
            });
            // $scope.recargarGuias();
        };
        resourceTransportista.getTransportistasCamion().ejecutar(function (data) {
            $scope.transportista = data.Transportistas;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
        $scope.PesoTotalFacturas = $scope.PESOTT;
        $scope.VolumenTotalFacturas = $scope.VOLTT;
        $scope.MontoTotalFacturas = $scope.MONTOTT;
        $scope.FacturasTotales = $scope.NFACT;
    };

    $scope.ArrayFacturaDelete = [];

    $scope.seleccionarFacturaConGuia = function ($index) {

        $scope.state = $scope.facturaConGuiaE[$index].status;
        console.log($scope.state);
        if ($scope.state) {

            //FACTURAS
            $scope.FacturasTotales = $scope.FacturasTotales - 1;
            $scope.ArrayFacturaDelete.push($scope.facturaConGuiaE[$index].Numerofactura);

            //NUMERO DE CLIENTES
            var result = $scope.ArrayClienteViejo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            $scope.ArrayClienteViejo.splice(result, 1);
            result = $scope.ArrayClienteViejo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            if (result === -1) {
                var borrar = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
                $scope.ArrayClienteNuevo.splice(borrar, 1);
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;
            //PESO
            $scope.PesoTotalFacturas = parseFloat($scope.PesoTotalFacturas) - parseFloat($scope.facturaConGuiaE[$index].PesoFactura);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);
            console.log($scope.pesoFixed);
            //VOLUMEN
            $scope.VolumenTotalFacturas = parseFloat($scope.VolumenTotalFacturas) - parseFloat($scope.facturaConGuiaE[$index].VolumentFactura);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);
            console.log($scope.VolumenTfixed);
            //MONTO TOTAL
            $scope.MontoTotalFacturas = parseFloat($scope.MontoTotalFacturas) - parseFloat($scope.facturaConGuiaE[$index].MontoTotal);
            console.log($scope.MontoTotalFacturas);
        } else {
            //OPERACIONES

            //FACTURAS TOTALES
            $scope.FacturasTotales = $scope.FacturasTotales + 1;
            var posini = $scope.ArrayFacturaDelete.indexOf($scope.facturaConGuiaE[$index].Numerofactura);
            $scope.ArrayFacturaDelete.splice(posini, 1);

            //CLIENTES
            $scope.ArrayClienteViejo.push($scope.facturaConGuiaE[$index].CodigoCliente);
            console.log($scope.ArrayClienteViejo);
            var result = $scope.ArrayClienteNuevo.indexOf($scope.facturaConGuiaE[$index].CodigoCliente);
            console.log(result)
            if (result >= 0) {
                console.log("no agregarlo");
                console.log($scope.ArrayClienteNuevo)
            } else {
                $scope.ArrayClienteNuevo.push($scope.facturaConGuiaE[$index].CodigoCliente);
                console.log("agregado");
                console.log($scope.ArrayClienteNuevo);
            }
            $scope.ClientesTotales = $scope.ArrayClienteNuevo.length;
            //PESO
            $scope.PesoTotalFacturas = parseFloat($scope.PesoTotalFacturas) + parseFloat($scope.facturaConGuiaE[$index].PesoFactura);
            $scope.pesoFixed = $scope.PesoTotalFacturas.toFixed(2);
            console.log($scope.pesoFixed);

            //VOLUMEN
            $scope.VolumenTotalFacturas = parseFloat($scope.VolumenTotalFacturas) + parseFloat($scope.facturaConGuiaE[$index].VolumentFactura);
            $scope.VolumenTfixed = $scope.VolumenTotalFacturas.toFixed(2);
            console.log($scope.VolumenTfixed);

            //MONTO TOTAL
            $scope.MontoTotalFacturas = parseFloat($scope.MontoTotalFacturas) + parseFloat($scope.facturaConGuiaE[$index].MontoTotal);
            console.log($scope.MontoTotalFacturas);
        }
    };
    //fin modificar guia despachada
});
