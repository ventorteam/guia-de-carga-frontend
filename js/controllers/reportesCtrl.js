/**
 * Created by PROGRAMADOR on 13/07/2016.
 */

app.controller('reportesCtrl', function ($scope, $http, $timeout, resourceTransportista) {

    //obtener todos los transportistas
    resourceTransportista.getAllTransportista().ejecutar(function (data) {
        $scope.transportista = data.Transportistas;
    }, function (error) {
        console.log(error);
    });

    //today
    $scope.fechaReporte = moment().format('DD-MM-YYYY');
    console.log($scope.fechaReporte);

    //reporte conteo

    //objeto de las opciones para filtrar el reporte
    $scope.opc = [
        {id: 1, name: 'Todos los productos'},
        {id: 2, name: 'Productos con existencia'}
    ];

    //ng change para escuchar el cambio en el select y llenar la variable opcConteo
    $scope.actSelect = function () {
        console.log($scope.opcConteo);
    };

    // metodo para obtener el reporte
    $scope.getReporteConteo = function () {

        if ($scope.opcConteo != undefined) {
            $http({
                url: api + '/reportes/getreporteconteo/' + $scope.opcConteo,
                method: "GET",
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data) {
                var blob1 = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                saveAs(blob1, 'Conteo ' + $scope.fechaReporte);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        } else {
            //alert('Debe seleccionar una opcion');
            UIkit.notify({
                message: 'Debe seleccionar una opcion',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
        }


    };

    //reporte inventario
    $scope.reporteInventario = console.log('');

    $scope.juan = function () {

        $scope.reporteInventario = '-';

        $.blockUI({message: '<img src="img/cargando.gif">'});

        $scope.reporteInventario = api + '/reportes/getReporteInventario';

        $timeout(function () {
            $.unblockUI();
        }, 25000);
    };


    //inicio reporte historico
    //funcion para rango de fecha KENDO.
    $('#start').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start = $("#start").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end = $("#end").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start.max(end.value());
    end.min(start.value());

    $scope.updateFiltro = function () {
        console.log($scope.filtro);
    };

    $scope.imprimirReporteHistorico = function () {

        //   debugger;
        var inicio = $("#start").val();
        var fin = $("#end").val();
        console.log(inicio + " " + fin);

        if ($scope.filtro != undefined) {
            $http({
                url: api + '/reportes/getReporteHistoricoPagos/' + inicio + '/' + fin + '/',
                method: "GET",
                headers: {
                    'Content-type': 'application/json',
                    'Cedula': $scope.filtro
                },
                responseType: 'blob'
            }).success(function (data) {
                console.log(data.size);

                if (data.size != 28) {
                    // debugger;
                    var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    saveAs(blob, 'Historico ' + $scope.fechaReporte);
                } else {
                    //alert('No hay datos en el rango seleccionado');
                    UIkit.notify({
                        message: 'No hay datos en el rango seleccionado',
                        status: 'warning',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                }


            }).error(function (data, status, headers, config) {
                //upload failed
            });
        } else {
            //alert('Debe seleccionar una opcion');
            UIkit.notify({
                message: 'Debe seleccionar una opcion',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
        }
    };


    //inicio reporte cantidad de pagos por fleteros
    //funcion para rango de fecha KENDO.
    $('#start2').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end2').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }


    var today = kendo.date.today();


    var start = $("#start2").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end = $("#end2").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start.max(end.value());
    end.min(start.value());

    $scope.tipofecha = [
        {id: 1, name: "Creacion"},
        {id: 2, name: "Despacho"}
    ];


    $scope.reporteCantiFlete = function () {

        var inicio2 = $("#start2").val();
        var fin2 = $("#end2").val();

        console.log(inicio2 + ' ' + fin2);

        if ($scope.filtro != undefined) {

            $http({
                url: api + '/reportes/getReporteCantidadFleteros/' + inicio2 + '/' + fin2,
                method: "GET",
                headers: {
                    'Content-type': 'application/json',
                    'Cedula': $scope.filtro
                },
                responseType: 'blob'
            }).success(function (data) {

                if (data.size != 38 && data.type != "application/json") {
                    console.log(data);
                    var blob2 = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    saveAs(blob2, 'Cantidad ' + $scope.fechaReporte);
                } else {
                    // alert('No hay datos en el rango seleccionado');
                    UIkit.notify({
                        message: 'No hay datos en el rango seleccionado',
                        status: 'warning',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                }
            }).error(function (data) {
                console.log(data);
            });

        } else {
            //alert('Debe seleccionar una opcion');
            UIkit.notify({
                message: 'Debe seleccionar una opcion',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
        }

    };


});