/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.controller('motivoCtrl', function ($scope, resourceMotivo) {

    resourceMotivo.getMotivo().ejecutar(function (data) {
        $scope.motivos = data.Motivos;
        console.log($scope.motivos);
    }, function (error) {
        console.log(error);
    });

    $scope.recargarMotivo = function () {
        resourceMotivo.getMotivo().ejecutar(function (data) {
            $scope.motivos = data.Motivos;
            console.log($scope.motivos);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.vaciarMotivo = function () {
        $scope.datosMotivo = {
            "motivo":
                    {
                        "descrip": $scope.descrip = ""
                    }
        };
    };

    $scope.postMotivo = function () {

        $scope.datosMotivo = {
            "motivo":
                    {
                        "descrip": $scope.descrip
                    }

        };

        $scope.motivoJSON = JSON.stringify($scope.datosMotivo);
        resourceMotivo.postMotivo().ejecutar($scope.motivoJSON, function (success) {
            UIkit.notify({
                message: 'Motivo creado con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });

            //console.log($scope.motivoJSON);
            $scope.recargarMotivo();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.deleteMotivo = function ($index) {
        UIkit.modal.confirm('Estas seguro(a)?', function () {
            resourceMotivo.deleteMotivo($scope.motivos[$index].id).ejecutar(function (success) {

                UIkit.notify({
                    message: 'Motivo eliminado con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });

                $scope.recargarMotivo();
            }, function (error) {
                UIkit.notify({
                    message: 'Error al eliminar motivo',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });
        });

    };
});