/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.controller('reportePagoCtrl', function ($scope, $route, resourcePago, resourceTransportista, resourceLogin, $timeout, $rootScope) {

    resourceTransportista.getAllTransportista().ejecutar(function (data) {
        $scope.transportista = data.Transportistas;
    }, function (error) {
        console.log(error);
    });

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $('#start').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start = $("#start").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end = $("#end").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start.max(end.value());
    end.min(start.value());

    $scope.RangoFechaPagos = function () {
        var inicio = $("#start").val();
        var fin = $("#end").val();
        $scope.idTransportista;
        console.log(inicio + " " + fin + " " + " " + $scope.idTransportista);

        resourcePago.getGuiasPagar(inicio, fin, $scope.idTransportista).ejecutar(function (data) {
            $scope.Pagos = data.GuiasPago;
            console.log(data);

        }, function (error) {
            console.log(error);
        });
    };
    //variables y arreglos, para seleccionarGuia()
    $scope.arrayGuias = [];
    $scope.arrayFull = [];
    $scope.montoTotalPago = 0;
    $scope.pesoTotalPago = 0;
    $scope.ClienteTotalPago = 0;
    $scope.BonoTotalPago = 0;
    $scope.baseTotalPago = 0;
    $scope.pagoTotalPago = 0;
    $scope.totalCajas = 0;
    $scope.totalClientes = 0;
    $scope.ivaTotalPago = 0;
    $scope.totalPago = 0;


    $scope.seleccionarGuia = function ($index) {
        $scope.state = $scope.Pagos[$index].status;
        console.log($scope.state);

        if ($scope.state) {

            $scope.JSONguias = {
                "ID": $scope.Pagos[$index].ID,
                "PAGOCLIE": $scope.Pagos[$index].PAGOCLIE,
                "PAGOBONO": $scope.Pagos[$index].PAGOBONO,
                "PAGOFLETE": $scope.Pagos[$index].PAGOFLETE,
                "BASE": $scope.Pagos[$index].BASE,
                "CAJASGUIA": $scope.Pagos[$index].CAJASGUIA,
                "PAGOIVA": $scope.Pagos[$index].IVA,
                "TOTALPAGOGUIA": $scope.Pagos[$index].TOTAL
            };

            $scope.arrayGuias.push($scope.JSONguias);

//            $scope.arrayGuias.push($scope.Pagos[$index].PAGOCLIE);
//            $scope.arrayGuias.push($scope.Pagos[$index].PAGOBONO);
//            $scope.arrayGuias.push($scope.Pagos[$index].PAGOFLETE);
//            $scope.arrayGuias.push($scope.Pagos[$index].PAGOIVA);
//            $scope.arrayGuias.push($scope.Pagos[$index].TOTALPAGOGUIA);

            console.log('Agrega ' + $scope.arrayGuias);

            $scope.montoTotalPago += parseFloat($scope.Pagos[$index].MONTOTT);
            //  console.log('Monto Total de las Guias ' + $scope.montoTotalPago);

            $scope.pesoTotalPago += Math.round(parseFloat($scope.Pagos[$index].PESOTT));
            // console.log('Peso Total de las Guias ' + $scope.pesoTotalPago);

            $scope.ClienteTotalPago += parseFloat($scope.Pagos[$index].PAGOCLIE);
            // console.log('Pago Cliente Total de las Guias ' + $scope.ClienteTotalPago);

            $scope.BonoTotalPago += parseFloat($scope.Pagos[$index].PAGOBONO);
            // console.log('Bono Total de las Guias ' + $scope.BonoTotalPago);

            $scope.pagoTotalPago += parseFloat($scope.Pagos[$index].PAGOFLETE);
            //  console.log('Pago Flete ' + $scope.pagoTotalPago);

            $scope.totalCajas += parseFloat($scope.Pagos[$index].CAJASGUIA);
            console.log('Cajas Total ' + $scope.totalCajas);

            $scope.totalClientes += parseInt($scope.Pagos[$index].NCLIE);
            console.log('Clientes Total ' + $scope.totalClientes);

            $scope.totalPago += parseFloat($scope.Pagos[$index].TOTAL);
            // console.log('Pago Total ' + $scope.totalPago);

            $scope.ivaTotalPago += $scope.Pagos[$index].IVA;
            // console.log('IVA Total ' + $scope.ivaTotalPago);

            $scope.baseTotalPago += parseFloat($scope.Pagos[$index].BASE);
            // console.log('Base Total ' + $scope.baseTotalPago);

        } else {

            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].PAGOCLIE);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].PAGOBONO);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].PAGOFLETE);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].PAGOIVA);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].TOTALPAGOGUIA);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].BASE);
            $scope.arrayGuias.splice($scope.posInit, 1);
            $scope.posInit = $scope.arrayGuias.indexOf($scope.Pagos[$index].CAJASGUIA);
            $scope.arrayGuias.splice($scope.posInit, 1);
            console.log('Elimina ' + $scope.arrayGuias);


            $scope.montoTotalPago = parseFloat($scope.montoTotalPago) - parseFloat($scope.Pagos[$index].MONTOTT);
            // console.log('Monto Total de las Guias ' + $scope.montoTotalPago);

            $scope.pesoTotalPago = Math.round(parseFloat($scope.pesoTotalPago)) - Math.round(parseFloat($scope.Pagos[$index].PESOTT));
            // console.log('Peso Total de las Guias ' + $scope.pesoTotalPago);

            $scope.ClienteTotalPago = parseFloat($scope.ClienteTotalPago) - parseFloat($scope.Pagos[$index].PAGOCLIE);
            // console.log('Pago Cliente Total de las Guias ' + $scope.ClienteTotalPago);

            $scope.BonoTotalPago = parseFloat($scope.BonoTotalPago) - parseFloat($scope.Pagos[$index].PAGOBONO);
            // console.log('Bono Total de las Guias ' + $scope.BonoTotalPago);

            $scope.pagoTotalPago = parseFloat($scope.pagoTotalPago) - parseFloat($scope.Pagos[$index].PAGOFLETE);
            //  console.log('Pago Flete Total ' + $scope.pagoTotalPago);

            $scope.totalCajas = parseFloat($scope.totalCajas) - parseFloat($scope.Pagos[$index].CAJASGUIA);
            console.log('Cajas Total ' + $scope.totalCajas);

            $scope.totalClientes = parseInt($scope.totalClientes) - parseInt($scope.Pagos[$index].NCLIE);
            console.log('Clientes Total ' + $scope.totalClientes);

            $scope.totalPago = $scope.totalPago - parseFloat($scope.Pagos[$index].TOTAL);
            //  console.log('Pago Total ' + $scope.totalPago);

            $scope.ivaTotalPago = $scope.ivaTotalPago - $scope.Pagos[$index].IVA;
            // console.log('IVA Total ' + $scope.ivaTotalPago);

            $scope.baseTotalPago = $scope.baseTotalPago - parseFloat($scope.Pagos[$index].BASE);
            //console.log('Base Total ' + $scope.baseTotalPago);
        }
    };

    $scope.crearPago = function () {
        $scope.fecha = moment().utc().format('YYYY-MM-DD');

        $scope.datosPago = {
            "pago": {
                "FECHAPAGO": $scope.fecha,
                "ID_TRANSPORTISTA": $scope.idTransportista,
                "KILOS": $scope.pesoTotalPago,
                "CAJAS": $scope.totalCajas,
                "CLIENTES": $scope.totalClientes,
                "TOTALCLIENTES": $scope.ClienteTotalPago,
                "TOTALGUIAS": $scope.montoTotalPago,
                "TOTALFLETES": $scope.pagoTotalPago,
                "TOTALBONOS": $scope.BonoTotalPago,
                "TOTALBASE": $scope.baseTotalPago,
                "TOTALIVA": $scope.ivaTotalPago,
                "TOTALPAGO": $scope.totalPago
            }
        };
        $scope.pagoJSON = JSON.stringify($scope.datosPago);
        $scope.guiaJSON = JSON.stringify($scope.arrayGuias);

        if (!$scope.arrayGuias.length <= 0) {
            resourcePago.postPago($scope.guiaJSON).ejecutar($scope.pagoJSON, function (success) {
                UIkit.notify({
                    message: 'Pago registrado con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
            }, function (error) {
                UIkit.notify({
                    message: 'Error al registrar el pago',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });
        }
        else {
            UIkit.notify({
                message: 'Debe seleccionar una guia.',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
            //alert('Debe seleccionar una guia.');
        }
    };

    //RANGO FECHAS PARA PAGOS
    $('#start2').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end2').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start2.value(),
            endDate = end2.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end2.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start2.max(endDate);
            end2.min(endDate);
        }
    }

    function endChange() {
        var endDate = end2.value(),
            startDate = start2.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start2.max(endDate);
        } else if (startDate) {
            end2.min(new Date(startDate));
        } else {
            endDate = new Date();
            start2.max(endDate);
            end2.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start2 = $("#start2").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end2 = $("#end2").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start2.max(end2.value());
    end2.min(start2.value());

    $scope.RangoGetPagos = function () {

        var inicio2 = $("#start2").val();
        var fin2 = $("#end2").val();
        console.log(inicio2 + " " + fin2);

        resourcePago.getPagos(inicio2, fin2).ejecutar(function (data) {
            $scope.gPago = data.PagosE;
            //$scope.reloadRoute();
            console.log($scope.gPago);
        }, function (error) {
            console.log(error);
        });
    };

    //vaciar campos
    $scope.vaciarLogin = function () {
        $scope.user = "";
        $scope.pass = "";
    };

//pago a visualizar
    $scope.seleccionarPago = function ($index) {

        console.log($scope.gPago[$index].IDPAGO);

        resourcePago.reportePago($scope.gPago[$index].IDPAGO).ejecutar(function (data) {
            $scope.getPago = data.ReportePago.GUIAS;
            $scope.getPagoPago = data.ReportePago;
            console.log($scope.getPago);
            console.log($scope.getPagoPago);
        }, function (error) {
            console.log(error);
        });

        $scope.IDPAGO = $scope.gPago[$index].IDPAGO;
        $scope.FECHAPAGO = $scope.gPago[$index].FECHAPAGO;
        $scope.ID_TRANSPORTISTA = $scope.gPago[$index].ID_TRANSPORTISTA;
        $scope.NOMBRE = $scope.gPago[$index].NOMBRE;
        $scope.APELLIDO = $scope.gPago[$index].APELLIDO;
        $scope.KILOS = $scope.gPago[$index].KILOS;
        $scope.CAJAS = $scope.gPago[$index].CAJAS;
        $scope.CLIENTES = $scope.gPago[$index].CLIENTES;
        $scope.TOTALCLIENTES = $scope.gPago[$index].TOTALCLIENTES;
        $scope.TOTALGUIAS = $scope.gPago[$index].TOTALGUIAS;
        $scope.TOTALBONOS = $scope.gPago[$index].TOTALBONOS;
        $scope.TOTALFLETES = $scope.gPago[$index].TOTALFLETES;
        $scope.TOTALBASE = $scope.gPago[$index].TOTALBASE;
        $scope.TOTALIVA = $scope.gPago[$index].TOTALIVA;
        $scope.TOTALPAGO = $scope.gPago[$index].TOTALPAGO;
        $scope.RIF = $scope.gPago[$index].RIF;
        $scope.NOMBEMPRESA = $scope.gPago[$index].NOMBEMPRESA;

//pago a imprimir
        $scope.imprimir = function ($index) {

            $.blockUI({message: '<img src="img/cargando.gif">'});
            $timeout(function () {
                // Only pt supported (not mm or in)
                var doc = new jsPDF('p', 'pt', 'letter');
                doc.text(40, 40, 'DISTRIBUCIONES LA PRINCIPAL LA GRITA C.A.');
                doc.setFontSize(9);
                doc.text(40, 60, 'Oficina Transporte:');
                doc.text(140, 60, '0276-3919003');
                doc.text(220, 60, '0414-0750569');
                doc.text(40, 80, 'Oficina Adminstracion:');
                doc.text(140, 80, '0276-3919003');
                doc.text(40, 100, 'Transportista: ' + $scope.NOMBRE + ' ' + $scope.APELLIDO);
                doc.text(40, 120, 'Cedula: ' + $scope.ID_TRANSPORTISTA);
                doc.text(40, 140, 'Empresa Transportista: ' + $scope.RIF + ' ' + $scope.NOMBEMPRESA);
                doc.text(440, 60, 'No. Pago: ' + $scope.IDPAGO);
                doc.text(440, 80, 'Fecha: ' + $scope.FECHAPAGO);

                var columns = [
                    {title: "N° Guia", dataKey: "ID"},
                    {title: "Fecha Creacion", dataKey: "FECHACREA"},
                    {title: "Zona", dataKey: "NOMBRE_CIUDAD"},
                    {title: "Kilos", dataKey: "PESOTT"},
                    {title: "Cajas", dataKey: "CAJASGUIA"},
                    {title: "Clientes", dataKey: "NCLIE"},
                    {title: "Cliente Efect.", dataKey: "PAGOCLIE"},
                    {title: "Total Bs. Guias", dataKey: "MONTOTT"},
                    {title: "Monto Flete", dataKey: "PAGOFLETE"},
                    {title: "Bono", dataKey: "PAGOBONO"},
                    {title: "Base", dataKey: "BASE"},
                    {title: "IVA", dataKey: "PAGOIVA"},
                    {title: "Total", dataKey: "TOTALPAGOGUIA"}
                ];

                /*var cols = getColumns();
                 cols.splice(0, 2);
                 doc.autoTable(cols, getData(40), {startY: 150});
                 */
                var a = columns.splice(0, 13);

                doc.autoTable(a, $scope.getPago, {
                    startY: 150,
                    theme: 'striped',
                    tableWidth: 'auto',
                    styles: {
                        fontSize: 7,
                        font: 'Arial',
                        lineWidth: 0.5,
                        rowHeight: 20,
                        overflow: 'linebreak',
                        valign: 'middle',
                        halign: 'center'
                    },
                    columnStyles: {
                        id: {fillColor: 99},
                        lineWidth: 2,
                        rowHeight: 20,
                        fontSize: 15,
                        font: 'Arial',
                        valign: 'middle',
                        halign: 'center',
                        overflow: 'linebreak'
                    }
//                    margin: {top: 140},
//                    pageBreak: 'auto',
//                    beforePageContent: function (data) {
//                    }
                });
                var columnas = [
                    "T. Kilos",
                    "T. Cajas",
                    "T. Clientes",
                    "T. Cliente Efect.",
                    "T. Bs. Guias",
                    "T. Monto Flete",
                    "T. Bono",
                    "T. Base",
                    "T. IVA",
                    "T.Pago"
                ];
                var rows = [
                    [
                        $scope.KILOS,
                        $scope.CAJAS,
                        $scope.CLIENTES,
                        $scope.TOTALCLIENTES,
                        $scope.TOTALGUIAS,
                        $scope.TOTALFLETES,
                        $scope.TOTALBONOS,
                        $scope.TOTALBASE,
                        $scope.TOTALIVA,
                        $scope.TOTALPAGO
                    ]
                ];
                doc.autoTable(columnas, rows, {
                    theme: 'striped',
                    tableWidth: 'auto',
                    styles: {
                        fontSize: 6.5,
                        font: 'Arial',
                        lineWidth: 0.5,
                        rowHeight: 20,
                        overflow: 'linebreak',
                        valign: 'middle',
                        halign: 'center'
                    },
                    columnStyles: {
                        id: {fillColor: 99},
                        lineWidth: 2,
                        rowHeight: 20,
                        fontSize: 15,
                        font: 'Arial',
                        valign: 'middle',
                        halign: 'center',
                        overflow: 'linebreak'
                    },
                    margin: {top: doc.autoTableEndPosY() + 10, left: 180},
                    beforePageContent: function (data) {
                    }
                });
                doc.save('reportePagoFleteros.pdf');
                $.unblockUI();
            }, 4000);
        };
    };
    //Eliminar pago
    $scope.eliminarPago = function ($index) {

        $scope.dataAuth = $scope.user + ':' + $scope.pass;
        resourceLogin.getAuthUser($scope.dataAuth).ejecutar(function (success) {
            $scope.executeAction = success.response;
            console.log(success.response);

            if ($scope.executeAction === 1) {
                resourcePago.deletePago($scope.IDPAGO).ejecutar(function (success) {
                    UIkit.notify({
                        message: 'Pago eliminado con exito',
                        status: 'success',
                        timeout: 3000,
                        pos: 'top-center'
                    });
                    console.log($scope.IDPAGO);
                    $scope.reloadRoute();
                });

            } else {
                UIkit.notify({
                    message: 'Usuario no autorizado.',
                    status: 'warning',
                    timeout: 3000,
                    pos: 'top-center'
                });
            }
        }, function (error) {
            UIkit.notify({
                message: 'Error al verificar las credenciales',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
            console.log(error);
        });
    };
});