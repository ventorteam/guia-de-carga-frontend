app.controller('tabuladorCtrl', function ($scope, resourceCiudades, resourceEstados, resourceGuiasDespachadas, resourceTabulador, $route) {


    resourceTabulador.getFletes().ejecutar(function (data) {
        $scope.fletes = data.Fletes;
    }, function (error) {
        console.log(error);
    });

//recarga los fletes
    $scope.recargarFletes = function () {
        resourceTabulador.getFletes().ejecutar(function (data) {
            $scope.fletes = data.Fletes;
        }, function (error) {
            console.log(error);
        });
    };

    //Reset para los select estado y ciudad
    $scope.resetTab = function () {
        if (angular.isDefined($scope.estadoObj, $scope.ciudadObj)) {
            delete $scope.estadoObj;
            delete $scope.ciudadObj;
        }
    };

    //Reinicia los campos para crear otro registro
    $scope.vaciarTab = function () {

        $scope.codigoesta = "";
        $scope.codigociud = "";
        $scope.NOMBCIUDAD = "";
        $scope.NOMBESTA = "";
        $scope.C1 = "";
        $scope.C2 = "";
        $scope.C3 = "";
        $scope.C4 = "";
        $scope.C5 = "";


    };

    resourceEstados.getEstados().ejecutar(function (data) {
        $scope.estados = data.estado;
    }, function (error) {
        console.log(error);
    });

    //Recarga la vista
    $scope.reloadRoute = function () {
        $route.reload();
    };

//Mostrar las ciudades segun el estado seleccionado
    $scope.convertir = function () {
        $scope.ObjEstado = JSON.parse($scope.estadoObj);
        resourceCiudades.getCiudades($scope.ObjEstado.CODIESTA).ejecutar(function (data) {
            console.log($scope.codigoesta);
            $scope.ciudades = data.ciudad;
        }, function (error) {
            $.unblockUI();
            alert("Seleccione una ciudad");
            $scope.reloadRoute();
            console.log(error);
        });
    };

    $scope.ftipocamion = {
        flete: {
            "C1": "0",
            "C2": "0",
            "C3": "0",
            "C4": "0",
            "C5": "0"
        }
    };

//POST Nuevo Tabulador de Precios por Ciudad
    $scope.postTabulador = function () {
        console.log($scope.ciudadObj);
        $scope.ObjCiudad = JSON.parse($scope.ciudadObj);

        $scope.ftipocamion = {
            flete: {
                "codesta": $scope.ObjEstado.CODIESTA,
                "codciud": $scope.ObjCiudad.CODICIUD,
                "nombciud": $scope.ObjCiudad.NOMBCIUD.trim(),
                "nombesta": $scope.ObjEstado.NOMBESTA.trim(),
                "C1": $scope.C1,
                "C2": $scope.C2,
                "C3": $scope.C3,
                "C4": $scope.C4,
                "C5": $scope.C5
            }
        };
        $scope.ftcJSON = JSON.stringify($scope.ftipocamion);
        resourceTabulador.postTipoF().ejecutar($scope.ftcJSON, function (success) {
            UIkit.notify({
                message: 'Ciudad creada con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.recargarFletes();
            console.log($scope.ftcJSON);
        }, function (error) {
            UIkit.notify({
                message: 'Ha ocurrido un error. Ciudad duplicada',
                status: 'warning',
                timeout: 3000,
                pos: 'top-center'
            });
            $.unblockUI();
            //alert("Ha ocurrido un error.");
            $scope.reloadRoute();
            console.log(error);
        });
    };

//Tabulador seleccionado
    $scope.editarTab = function ($index) {

        $scope.codesta = $scope.fletes[$index].codesta;
        $scope.codciud = $scope.fletes[$index].codciud;
        $scope.nombciud = $scope.fletes[$index].nombciud;
        $scope.nombesta = $scope.fletes[$index].nombesta;
        $scope.C1_mod = $scope.fletes[$index].C1;
        $scope.C2_mod = $scope.fletes[$index].C2;
        $scope.C3_mod = $scope.fletes[$index].C3;
        $scope.C4_mod = $scope.fletes[$index].C4;
        $scope.C5_mod = $scope.fletes[$index].C5;
    };

//Actualizar los montos del tabulador
    $scope.updateTab = function ($index) {
        $scope.tabMod = {
            flete: {
                "C1": $scope.C1_mod,
                "C2": $scope.C2_mod,
                "C3": $scope.C3_mod,
                "C4": $scope.C4_mod,
                "C5": $scope.C5_mod
            }
        };
        $scope.tabJSON = JSON.stringify($scope.tabMod);

        resourceTabulador.updateFletes($scope.codesta, $scope.codciud).ejecutar($scope.tabJSON, function (success) {
            UIkit.notify({
                message: 'Ciudad actualizado con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            console.log($scope.tabJSON);
            $scope.recargarFletes();
        }, function (error) {
            UIkit.notify({
                message: 'Error al actualizar.',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            console.log(error);
        });
    };

    //Eliminar Tabulador
    $scope.deleteTab = function ($index) {
        UIkit.modal.confirm('Estas seguro(a)?', function () {
            resourceTabulador.deleteFletes($scope.fletes[$index].codesta, $scope.fletes[$index].codciud).ejecutar(function (success) {
                UIkit.notify({
                    message: 'Ciudad eliminada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.reloadRoute();
                $scope.recargarFletes();

            }, function (error) {
                UIkit.notify({
                    message: 'Error al eliminar ciudad',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });

        });
    };

});
