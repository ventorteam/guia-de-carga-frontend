/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.controller('duenoCtrl', function ($scope, resourceDueno, resourceZona, resourceGuias, $route) {

    $scope.reloadRoute = function () {
        $route.reload();
    };

    //Obtener todos los dueños
    resourceDueno.getAllDuenos().ejecutar(function (data) {
        $scope.dueno = data.Duenos;
    }, function (error) {
        console.log(error);
    });

    //Obtener todas las zonas sin duenos 
    resourceZona.getZonasSinDueno().ejecutar(function (data) {
        $scope.zona = data.ZonasSinDuenos;
    }, function (error) {
        console.log(error);
    });

    $scope.recargarDuenos = function () {
        resourceDueno.getAllDuenos().ejecutar(function (data) {
            $scope.dueno = data.Duenos;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.recargarZona = function () {
        //Obtener todas las zonas sin duenos 
        resourceZona.getZonasSinDueno().ejecutar(function (data) {
            $scope.zona = data.ZonasSinDuenos;
            console.log($scope.zona);
        }, function (error) {
            console.log(error);
        });
    };

    $scope.vaciardueno = function () {

        $scope.id = "";
        $scope.nombre = "";
        $scope.apellido = "";
        $scope.cargo = "";
    };

    //crear nuevo dueño
    $scope.seleccion = [];
    $scope.postDueno = function ($index) {
        $scope.datos = {
            "dueno": {
                "id": $scope.id,
                "nombre": $scope.nombre,
                "apellido": $scope.apellido,
                "cargo": $scope.cargo
            }
        };
        //debugger;
        $scope.datosjson = JSON.stringify($scope.datos);
        //console.log($scope.seleccion);
        if ($scope.seleccion.length > 0) {
            resourceDueno.postDueno().ejecutar($scope.datosjson, function (success) {
                $scope.recargarDuenos();

                for (var i = 0, max = $scope.seleccion.length; i < max; i++) {
                    $scope.datosZona = {
                        duenozona: {
                            "id_dueno": $scope.datos.dueno.id,
                            "id_zona": $scope.seleccion[i]
                        }
                    };
                    $scope.datosZonaJson = JSON.stringify($scope.datosZona);
                    resourceZona.postZonaDueno().ejecutar($scope.datosZonaJson, function (success) {
                        UIkit.notify({
                            message: 'Dueño creado',
                            status: 'success',
                            timeout: 3000,
                            pos: 'top-center'
                        });
                        $scope.vaciardueno();
                        $scope.recargarZona();

                    }, function (error) {
                        console.log(error);
                    });
                }
            }, function (error) {
                $.unblockUI();
                console.log(error);
               // alert("Ha ocurrido un error, Dueño duplicado");
                UIkit.notify({
                    message: 'Ha ocurrido un error, Dueño duplicado',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.reloadRoute();
            });
        }else{
           // alert('Debe seleccionar al menos una zona para crear un dueño');
            UIkit.notify({
                message: 'Debe seleccionar al menos una zona para crear un dueño',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
        }
    };

    $scope.selectZona = function ($index) {
        $scope.state = $scope.zona[$index].estatus;
        if ($scope.state) {
            console.log("es true");
            $scope.seleccion.push($scope.zona[$index].id);
            console.log($scope.seleccion);
        } else {
            $scope.posini = $scope.seleccion.indexOf($scope.zona[$index].id);
            $scope.seleccion.splice($scope.posini, 1);
        }
    };

    //modificar dueño excepto cedula
    $scope.modificar = function ($index) {

        $scope.cedula = $scope.dueno[$index].id;
        $scope.nombre_mod = $scope.dueno[$index].nombre;
        $scope.apellido_mod = $scope.dueno[$index].apellido;
        $scope.cargo_mod = $scope.dueno[$index].cargo;
        $scope.obtenerZonaConDueno($scope.cedula);

    };

    $scope.modificarDueno = function ($index) {
        $scope.datos_mod = {
            "dueno": {
                "id": $scope.cedula,
                "nombre": $scope.nombre_mod,
                "apellido": $scope.apellido_mod,
                "cargo": $scope.cargo_mod
            }
        };
        $scope.datojson_mod = JSON.stringify($scope.datos_mod);
        console.log($scope.datos_mod);
        resourceDueno.updateDueno($scope.cedula).ejecutar($scope.datojson_mod, function (success) {

            UIkit.notify({
                message: 'Dueño actualizado',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.recargarDuenos();

        }, function (error) {

        });
    };


    $scope.deleteDueno = function ($index) {

        UIkit.modal.confirm('Estas seguro(a)?', function () {
            // console.log($scope.dueno[$index].id);
            resourceDueno.deleteDueno($scope.dueno[$index].id).ejecutar(function (success) {
                UIkit.modal.alert('Eliminado!');
                $scope.recargarDuenos();
                $scope.recargarZona();
                $scope.reloadRoute();
            }, function (error) {

            });

        });

    };

    $scope.reiniciarZonaDueno = function () {
        console.log("borrado array");
        $scope.arrayZonasConDueno = [];
        $scope.seleccion = [];
    };
    $scope.arrayZonasConDueno = [];
    $scope.obtenerZonaConDueno = function (CI) {
        resourceDueno.getZonaConDueno().ejecutar(CI, function (success) {
            //console.log('Esto deberia ser el arreglo con zonas' + success);
        }, function (error) {

        });
    };
//     $scope.imprimir = function () {
//
//         var data = [], fontSize = 7, height = 200, doc;
//         doc = new jsPDF('p', 'pt', 'a4', true);
//         // doc.setFont("times", "normal");
//
//         doc.text(40, 40, 'DISTRIBUCIONES PRINCIPAL C.A.');
//
//         doc.setFontSize(9);
//         doc.text(460, 40, 'Fecha:');
//         doc.text(500, 40, '{{fecha}}');
//         doc.text(460, 60, 'No. Guia:');
//         doc.text(500, 60, '{{Numero}}');
//
//         doc.text(40, 80, 'Oficina Transporte:');
//         doc.text(140, 80, '0276-3919003');
//         doc.text(220, 80, '0414-0750569');
//         doc.text(40, 100, 'Oficina Adminstracion:');
//         doc.text(140, 100, '0276-3919003');
//
//         doc.setFontSize(8);
//         doc.text(40, 120, 'Total Factura:');
//         doc.text(100, 120, '{TF}');
//
//         doc.text(140, 120, 'Total Clientes:');
//         doc.text(200, 120, '{TC}');
//
//         doc.text(240, 120, 'Total Peso:');
//         doc.text(290, 120, '{TP}');
//
//         doc.text(330, 120, 'Total Volumen:');
//         doc.text(400, 120, '{TV}');
//
//
// //        doc.setFontSize(fontSize);
// //        data = doc.tableToJson('tableFacturas');
// //        height = doc.drawTable(data, {
// //            xstart: 40,
// //            ystart: 130,
// //            tablestart: 130,
// //            marginleft: 5,
// //            xOffset: 7,
// //            yOffset: 7,
// //            columnWidths: [10, 10, 80, 20, 20, 20, 20, 20, 20]
// //        });
//
//         doc.setFont("times", "normal");
//
//         doc.setFontSize(9);
//         doc.text(40, height + 40, 'Nombre del Chofer:   ___________________________');
//         doc.text(260, height + 40, 'Despachado:   ___________________________');
//
//         doc.text(40, height + 60, 'Camion y Placa:   ___________________________');
//         doc.text(260, height + 60, 'Entregado:   ___________________________');
//
//         doc.text(40, height + 80, 'Cedula:   ___________________________');
//         doc.text(260, height + 80, 'Recibido por:   ___________________________');
//
//         doc.addPage();
//         doc.setFont("arial", "normal");
//         doc.setFontSize(9);
//
//         doc.text(40, 60, 'No Guia');
//         doc.text(40, 80, '{{Numero}}');
//
//         doc.setFontSize(8);
//         doc.text(250, 40, 'Zona de Despacho');
//         doc.setFontSize(9);
//         doc.text(250, 60, 'Rubio');
//
//         doc.setFontSize(8);
//         doc.text(400, 40, 'Dueño por Zona');
//         doc.text(500, 40, 'Zona');
//         doc.text(400, 60, '{{Dueño Zona}}');
//         doc.text(500, 60, '{{3}}');
//
//
//         doc.text(40, 120, 'Total Factura:');
//         doc.text(100, 120, '{TF}');
//
//         doc.text(140, 120, 'Total Clientes:');
//         doc.text(200, 120, '{TC}');
//
//         doc.text(240, 120, 'Total Peso:');
//         doc.text(290, 120, '{TP}');
//
//         doc.text(330, 120, 'Total Volumen:');
//         doc.text(400, 120, '{TV}');
//
// //        doc.setFontSize(fontSize);
// //        data = doc.tableToJson('tablaFacturas');
// //        height = doc.drawTable(data, {
// //            xstart: 40,
// //            ystart: 130,
// //            tablestart: 130,
// //            marginleft: 5,
// //            xOffset: 7,
// //            yOffset: 7,
// //            columnWidths: [10, 10, 80, 20, 20, 20, 20, 20, 20]
// //        });
//         doc.save("some-file.pdf");
//     };

});
