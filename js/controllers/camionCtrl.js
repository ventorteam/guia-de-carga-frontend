/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
app.controller('camionCtrl', function ($scope, resourceCamion, resourceTipoCamion, resourceTransportista, $route) {

    resourceCamion.getCamionesContransportista().ejecutar(function (data) {
        $scope.camion = data.Camiones;
        // console.log(data.Camiones);
    }, function (error) {
        console.log(error);
    });
    resourceTipoCamion.getAllTipoCamiones().ejecutar(function (data) {
        $scope.tcamion = data.Camiones;
        // console.log(data.Camiones);
    }, function (error) {
        //console.log(error);
    });
    resourceTransportista.getAllTransportista().ejecutar(function (data) {
        $scope.transportista = data.Transportistas;
        // console.log(data.Transportistas);
    }, function (error) {
        // console.log(error);
    });
    $scope.recargarCamion = function () {
        resourceCamion.getCamionesContransportista().ejecutar(function (data) {
            $scope.camion = data.Camiones;
            //  console.log(data.Camiones);
        }, function (error) {
            //  console.log(error);
        });
    };

    $scope.vaciarCamion = function () {
        $scope.datoscamion = {
            "camion": {
                "placa": $scope.placa = "",
                "descripcion": $scope.descripcion = "",
                "capacidad": $scope.capacidad = "",
                "tipocamion_id": $scope.tipocamion_id = "",
                "transportista_id": $scope.transportista_id = ""

            }
        };
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };


    $scope.PostCamion = function () {
        console.log($scope.transportista_id);
        $scope.datoscamion = {
            "camion": {
                "placa": $scope.placa,
                "descripcion": $scope.descripcion,
                "capacidad": $scope.capacidad,
                "transportista_id": $scope.transportista_id
            }
        };

        $scope.datajson = JSON.stringify($scope.datoscamion);
        resourceCamion.postCamion().ejecutar($scope.datajson, function (success) {
            UIkit.notify({
                message: 'Camion creado con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            //console.log("esto es succes=" + success);
            $scope.recargarCamion();
            $scope.vaciarCamion();

            console.log($scope.datajson);
        }, function (data) {
            $.unblockUI();
            //console.log(data.data.error);
            if ((data.data.error) === 0) {
                // alert("Ha ocurrido un error. Capacidad de Camión Invalida");
                UIkit.notify({
                    message: 'Ha ocurrido un error. Capacidad de Camión Invalida',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
            } else {
                //  alert("Ha ocurrido un error. Camión Duplicado");
                UIkit.notify({
                    message: 'Ha ocurrido un error. Camión Duplicado',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.reloadRoute();
//            console.log(error);
            }
        });
    };

    $scope.modificarCamion = function ($index) {

        $scope.vaciarCamion();

        $scope.placa = $scope.camion[$index].placa;
        $scope.descripcion_mod = $scope.camion[$index].descripcion;
        $scope.capacidad_mod = Math.floor($scope.camion[$index].capacidad);
        $scope.tipocamion_id = $scope.camion[$index].tipocamion_id;
        $scope.transportista_id_mod = $scope.camion[$index].transportista_id;

    };
    $scope.updateCamion = function () {
        $scope.camion_mod = {
            camion: {
                "placa": $scope.placa,
                "descripcion": $scope.descripcion_mod,
                "capacidad": Math.floor($scope.capacidad_mod),
                "tipocamion_id": $scope.tipocamion_id_mod,
                "transportista_id": $scope.transportista_id_mod
            }
        };
        $scope.camion_modjson = JSON.stringify($scope.camion_mod);
        resourceCamion.updateCamion($scope.placa).ejecutar($scope.camion_modjson, function (success) {
            $scope.recargarCamion();
        }, function (error) {
            console.log(error);
        });

    };

    $scope.deleteCamion = function ($index) {
        UIkit.modal.confirm('Estas seguro(a)?', function () {
            resourceCamion.deleteCamion($scope.camion[$index].placa).ejecutar(function (success) {

                UIkit.notify({
                    message: 'Ha ocurrido un error. Camión Duplicado',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });

                $scope.recargarCamion();
                $scope.reloadRoute();

            });
            UIkit.modal.alert('Eliminado!');
        });
    };
});



