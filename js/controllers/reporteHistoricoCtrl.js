/**
 * Created by PROGRAMADOR on 13/06/2016.
 */
app.controller('reporteHistoricoCtrl', function ($scope, resourceHistorico, $http, $timeout) {

    //funcion para rango de fecha KENDO.
    $('#start').click(function () {
        $('span[aria-controls="start_dateview"]').trigger("click");
    });
    $('#end').click(function () {
        $('span[aria-controls="end_dateview"]').trigger("click");
    });

    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start = $("#start").kendoDateTimePicker({
        value: today,
        change: startChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    var end = $("#end").kendoDateTimePicker({
        value: today,
        change: endChange,
        format: "yyyy-MM-dd"
    }).data("kendoDateTimePicker");

    start.max(end.value());
    end.min(start.value());

    //Funcion para retornar el reporte segun rango de fecha
    $scope.HistoricoRango = function () {
        //   debugger;
        var inicio = $("#start").val();
        var fin = $("#end").val();
        console.log(inicio + " " + fin);

        $http({
            url: api +  '/reportes/getReporteHistoricoPagos/' + inicio + '/' + fin + '/',
            method: "GET",
            headers: {
                'Content-type': 'application/json'
            },
            responseType: 'arraybuffer'
        }).success(function (data) {
            var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
            saveAs(blob, 'Historico');
        }).error(function (data, status, headers, config) {
            //upload failed
        });
    };

});