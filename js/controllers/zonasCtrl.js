/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.controller('zonasCtrl', function ($scope, resourceZona, $route) {
    resourceZona.getAllZona().ejecutar(function (success) {
        $scope.zonas = success.zonas;
    }, function (error) {
        alert("error\n" + error);
    });

    //Obtener zonas sin ubicacion
    resourceZona.getZonaSinUbic().ejecutar(function (success) {
        $scope.UbicSinZona = success.UbicSinZona;
        console.log($scope.UbicSinZona);
    }, function (error) {
        console.log(error);
    });

    $scope.arrayUbicaciones = [];
    //Seleccionando zonas
    $scope.SeleccionarZonas = function ($index) {
        $scope.state = $scope.UbicSinZona[$index].status; //con el metodo status devuelve si es true o false por el checkbox
        console.log($scope.state);
        if ($scope.state) {
            $scope.arrayUbicaciones.push($scope.UbicSinZona[$index].codiubic);
            console.log($scope.arrayUbicaciones);
        } else {
            $scope.posini = $scope.arrayUbicaciones.indexOf($scope.UbicSinZona[$index].codiubic); //Saber el indice de este codiubic
            $scope.arrayUbicaciones.splice($scope.posini, 1);
        }
    };

    $scope.vaciarZona = function () {
        $scope.nombre = "";

    };
    //Guardando zonas
    $scope.postZonas = function () {
        $scope.datosNewZona = {
            "zona": {
                "nombre": $scope.nombre
            }
        };
        $scope.zonajson = JSON.stringify($scope.datosNewZona);
        if (!$scope.arrayUbicaciones.length <= 0) {
            resourceZona.postZona().ejecutar($scope.zonajson, function (success) {
                UIkit.notify({
                    message: 'Zona creada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
               // console.log('GUARDO CORRECTAMENTE lo siguiente ' + success);
                $scope.nombre = '';

                //Obtener ID de la proxima Zona
                resourceZona.getZonaID().ejecutar(function (success) {
                    $scope.var_id_zona = (success.numero.AUTO_INCREMENT - 1);
                    for (var i = 0; i < $scope.arrayUbicaciones.length; i++) {
                        $scope.datosNewZonaUbic = {
                            "zonaubic": {
                                "id_zona": $scope.var_id_zona,
                                "id_ubic": $scope.arrayUbicaciones[i]
                            }
                        };

                        $scope.datosNewZonaUbicjson = JSON.stringify($scope.datosNewZonaUbic);
                        resourceZona.postZonaUbicacion().ejecutar($scope.datosNewZonaUbicjson, function (success) {
                            $scope.RecargarUbicaciones();

                            console.log('Guardado correctamente ');

                        }, function (error) {
                            console.log('El error ' + error);
                        });

                    }
                    console.log("EL AUTO INCREMENT ES " + (success.numero.AUTO_INCREMENT - 1));

                }, function (error) {
                    console.log("Error " + error);
                });
                $scope.recargarZonas();
            }, function (error) {
                console.log('El error es el siguiente ' + error);
            });
        } else {
            //alert('Debe elegir al menos una ubicacion!');
            UIkit.notify({
                message: 'Debe elegir al menos una ubicacion!',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
        }
    };

    //Recargar zonas
    $scope.RecargarUbicaciones = function () {
        $scope.arrayUbicaciones = [];
        resourceZona.getZonaSinUbic().ejecutar(function (success) {
            $scope.UbicSinZona = success.UbicSinZona;
        }, function (error) {
            console.log('ERROR! ' + error);
        });
    };

    //Obteniendo datos al dar click en 'eDITAR ''
    $scope.modificar = function ($index) {
        $scope.idzona_mod = $scope.zonas[$index].ID;
        $scope.nombre_mod = $scope.zonas[$index].NOMBRE;
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };

    //Borrar zonas
    $scope.eliminarZona = function ($index) {
        UIkit.modal.confirm('Estas seguro(a)?', function () {
            resourceZona.deleteZona($scope.zonas[$index].ID).ejecutar(function (success) {
                UIkit.notify({
                    message: 'Zona eliminada con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });                $scope.recargarZonas();
                $scope.RecargarUbicaciones();
                $scope.reloadRoute();
            }, function (error) {
                UIkit.notify({
                    message: 'Error al eliminar zona',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log('encontro error');
            });
        });
    };

    //Recargar zonas
    $scope.recargarZonas = function () {
        resourceZona.getAllZona().ejecutar(function (success) {
            $scope.zonas = success.zonas;
        }, function (error) {
            console.log('Error recargando zonas ' + error);
        });
    };

    //
    // $scope.pruebaeddie = function(){
    //   resourceZona.prueba().ejecutar(function(data){
    //       debugger;
    //       console.log(data);
    //      
    //   }, function(error){
    //       console.log(error);
    //   });
    //    
    // };

});


