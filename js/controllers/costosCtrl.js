app.controller('costosCtrl', function ($scope, resourceCosto, $route) {

    resourceCosto.getCostos().ejecutar(function (data) {
        $scope.costo = data.Costo;
    }, function (error) {
        console.log(error);
    });

    $scope.recargarCosto = function () {
        resourceCosto.getCostos().ejecutar(function (data) {
            $scope.costo = data.Costo;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.vaciarCosto = function () {
        $scope.datosCosto = {
            costo: {
                "idcosto": $scope.idcosto = "",
                "cliente": $scope.cliente = "",
                "bono": $scope.bono = ""
            }
        };
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.modificarCosto = function ($index) {

        $scope.idcosto_mod = $scope.costo[$index].idcosto;
        $scope.cliente_mod = $scope.costo[$index].cliente;
        $scope.bono_mod = $scope.costo[$index].bono;
    };

    $scope.actCostos = function () {
        // debugger;
        $scope.datosCosto = {
            costo: {
                "cliente": $scope.cliente_mod,
                "bono": $scope.bono_mod
            }
        };
        $scope.datosJSON = JSON.stringify($scope.datosCosto);
        console.log($scope.datosJSON);
        resourceCosto.updateCostos($scope.idcosto_mod).ejecutar($scope.datosJSON, function (success) {
            console.log($scope.idcosto);
            console.log($scope.datosJSON);
            $scope.recargarCosto();
            UIkit.notify({
                message: 'Costo actualizado',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
        }, function (error) {
            $.unblockUI();
            console.log(error);
            // alert("Debe ingresar un monto valido");
            UIkit.notify({
                message: 'Debe ingresar un monto validos',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.reloadRoute();
            console.log(error);
        });
    };

});
