/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
app.controller('guiasReporteCarga', function ($scope) {

    //TABS
  

    $scope.imprimir = function () {


        var data = [], fontSize = 7, height = 200, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        // doc.setFont("times", "normal");
           
        doc.text(40, 40, 'DISTRIBUCIONES PRINCIPAL C.A.');

        doc.setFontSize(9);
        doc.text(460, 40, 'Fecha:');
        doc.text(500, 40, '{{fecha}}');
        doc.text(460, 60, 'No. Guia:');
        doc.text(500, 60, '{{Numero}}');

        doc.text(40, 80, 'Oficina Transporte:');
        doc.text(140, 80, '0276-3919003');
        doc.text(220, 80, '0414-0750569');
        doc.text(40, 100, 'Oficina Adminstracion:');
        doc.text(140, 100, '0276-3919003');

        doc.setFontSize(8);
        doc.text(40, 120, 'Total Factura:');
        doc.text(100, 120, '{TF}');

        doc.text(140, 120, 'Total Clientes:');
        doc.text(200, 120, '{TC}');

        doc.text(240, 120, 'Total Peso:');
        doc.text(290, 120, '{TP}');

        doc.text(330, 120, 'Total Volumen:');
        doc.text(400, 120, '{TV}');



        doc.setFontSize(fontSize);
        data = doc.tableToJson('tablaFactura');
        height = doc.drawTable(data, {
            xstart: 40,
            ystart: 130,
            tablestart: 130,
            marginleft: 5,
            xOffset: 7,
            yOffset: 7,
            columnWidths: [10, 10, 80, 20, 20, 20, 20, 20, 20]
        });

        doc.setFont("times", "normal");

        doc.setFontSize(9);
        doc.text(40, height + 40, 'Nombre del Chofer:   ___________________________');
        doc.text(260, height + 40, 'Despachado:   ___________________________');

        doc.text(40, height + 60, 'Camion y Placa:   ___________________________');
        doc.text(260, height + 60, 'Entregado:   ___________________________');

        doc.text(40, height + 80, 'Cedula:   ___________________________');
        doc.text(260, height + 80, 'Recibido por:   ___________________________');

        doc.addPage();
        doc.setFont("arial", "normal");
        doc.setFontSize(9);

        doc.text(40, 60, 'No Guia');
        doc.text(40, 80, '{{Numero}}');

        doc.setFontSize(8);
        doc.text(250, 40, 'Zona de Despacho');
        doc.setFontSize(9);
        doc.text(250, 60, 'Rubio');

        doc.setFontSize(8);
        doc.text(400, 40, 'Dueño por Zona');
        doc.text(500, 40, 'Zona');
        doc.text(400, 60, '{{Dueño Zona}}');
        doc.text(500, 60, '{{3}}');



        doc.text(40, 120, 'Total Factura:');
        doc.text(100, 120, '{TF}');

        doc.text(140, 120, 'Total Clientes:');
        doc.text(200, 120, '{TC}');

        doc.text(240, 120, 'Total Peso:');
        doc.text(290, 120, '{TP}');

        doc.text(330, 120, 'Total Volumen:');
        doc.text(400, 120, '{TV}');

        doc.setFontSize(fontSize);
        data = doc.tableToJson('tablaProductos');
        height = doc.drawTable(data, {
            xstart: 40,
            ystart: 130,
            tablestart: 130,
            marginleft: 5,
            xOffset: 7,
            yOffset: 7,
            columnWidths: [10, 10, 80, 20, 20, 20, 20, 20, 20]
        });


        doc.save("some-file.pdf");


    };


    function startChange() {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var today = kendo.date.today();

    var start = $("#start").kendoDateTimePicker({
        value: today,
        change: startChange,
        parseFormats: ["MM/dd/yyyy"]
    }).data("kendoDateTimePicker");

    var end = $("#end").kendoDateTimePicker({
        value: today,
        change: endChange,
        parseFormats: ["MM/dd/yyyy"]
    }).data("kendoDateTimePicker");

    start.max(end.value());
    end.min(start.value());

});

