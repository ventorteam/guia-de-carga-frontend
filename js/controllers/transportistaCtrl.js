/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app.controller('transportistaCtrl', function ($scope, resourceTransportista, $route, resourceEmpresa) {

//Obtener todos los Transportistas

    resourceTransportista.getAllTransportista().ejecutar(function (data) {
        $scope.transportista = data.Transportistas;
    }, function (error) {
       // console.log(error);
    });

    $scope.recargarTransportistas = function () {

        resourceTransportista.getAllTransportista().ejecutar(function (data) {
            $scope.transportista = data.Transportistas;
        }, function (error) {
          //  console.log(error);
        });
    };

    $scope.vaciarTransportista = function () {
        $scope.datos = {
            "transportista": {
                "id": $scope.id = "",
                "nombre": $scope.nombre = "",
                "apellido": $scope.apellido = "",
                "cargo": $scope.cargo = "",
                "empresa_id": $scope.empresa_id = "",
            }
        };

    };

    $scope.postTransportista = function () {
        $scope.datos = {
            transportista: {
                "id": $scope.id,
                "nombre": $scope.nombre,
                "apellido": $scope.apellido,
                "cargo": $scope.cargo,
                "empresa_id": $scope.empresa_id
            }
        };
        $scope.jsontransportista = JSON.stringify($scope.datos);
        console.log($scope.datos);

        resourceTransportista.setTransportista().ejecutar($scope.jsontransportista, function (data) {
            UIkit.notify({
                message: 'Transportista creado con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            //console.log("respuesta " + JSON.stringify(data));
            $scope.recargarTransportistas();
            // $scope.vaciarTransportistas();
        }, function (error) {
            $.unblockUI();
            UIkit.notify({
                message: 'Error al crear transportista',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
           // alert("Ha ocurrido un error. Transportista duplicado");
            $scope.reloadRoute();
            console.log(error);
        });
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.deleteTransportista = function ($index) {

        UIkit.modal.confirm('Estas seguro(a)?', function () {
            // console.log($scope.dueno[$index].id);
            resourceTransportista.deleteTransportista($scope.transportista[$index].id).ejecutar(function (success) {
                UIkit.notify({
                    message: 'Transportista eliminado con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                //UIkit.modal.alert('Eliminado!');
                $scope.recargarTransportistas();
                $scope.reloadRoute();
            }, function (error) {
                console.log(error);

            });

        });
    };
    //modificar excepto cedula
    $scope.modificarTransportista = function ($index) {

        $scope.cedula = $scope.transportista[$index].id;
        $scope.nombreT_mod = $scope.transportista[$index].nombre;
        $scope.apellidoT_mod = $scope.transportista[$index].apellido;
        $scope.cargoT_mod = $scope.transportista[$index].cargo;
        $scope.empresa_id_mod = $scope.transportista[$index].empresa_id;
    };

    $scope.updateTransportista = function () {
        $scope.datosT_mod = {
            transportista: {
                "id": $scope.cedula,
                "nombre": $scope.nombreT_mod,
                "apellido": $scope.apellidoT_mod,
                "cargo": $scope.cargoT_mod,
                "empresa_id": $scope.empresa_id_mod

            }
        };
        $scope.datosT_modjson = JSON.stringify($scope.datosT_mod);
        console.log($scope.datosT_mod);

        resourceTransportista.updateTransportista($scope.cedula).ejecutar($scope.datosT_modjson, function (success) {
            UIkit.notify({
                message: 'Transportista actualizado con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.recargarTransportistas();
            $scope.vaciarTransportista();
        }, function (error) {
            UIkit.notify({
                message: 'Error al actualizar transportista',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            console.log(error);
        });

    };

    //lista de empresar
    $scope.obtenerEmpresas = function () {

        resourceEmpresa.getEmpresa().ejecutar(function (data) {
            $scope.Empresa = data.Empresas;
            console.log($scope.Empresa);
        }, function (error) {
            console.log(error);
        });
    };

    //escucha el cambio en el select crear
    $scope.changeEmp = function () {
        $scope.empresa_id;
    };

    //escucha el cambio en el select actualizar
    $scope.changeEmpAct = function () {
        $scope.empresa_id_mod;
    };


});