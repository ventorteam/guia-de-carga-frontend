/**
 * Created by PROGRAMADOR on 09/08/2016.
 */
app.controller('empresaCtrl', function ($scope, resourceEmpresa, $route) {


    $scope.obtenerEmpresas = function () {

        resourceEmpresa.getEmpresa().ejecutar(function (data) {
            $scope.Empresa = data.Empresas;
            console.log($scope.Empresa);
        }, function (error) {
            console.log(error);
        });
    };
    $scope.obtenerEmpresas();

    //function para vaciar los datos los input
    $scope.vaciarEmpresa = function () {
        $scope.objEmpresa = {
            "empresa": {
                "id": $scope.rif = "",
                "nombre": $scope.nombre = "",   
                "direccion": $scope.dir = ""
            }
        };
    };

    $scope.crearEmpresa = function () {
        //   debugger;
        $scope.objEmpresa = {
            "empresa": {
                "id": $scope.rif,
                "nombre": $scope.nombre,
                "direccion": $scope.dir
            }
        };
        $scope.objEmpresaJSON = JSON.stringify($scope.objEmpresa);

        console.log('Objeto Empresa ' + $scope.objEmpresa);
        console.log('Objeto Empresa JSON ' + $scope.objEmpresaJSON);

        resourceEmpresa.postEmpresa().ejecutar($scope.objEmpresaJSON, function (success) {
            console.log(success);
            $scope.obtenerEmpresas();
            $scope.vaciarEmpresa();
            //Notificacion de uikit
            UIkit.notify({
                message: 'Empresa creada con exito',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });

        }, function (error) {
            console.log(error);
            //Notificacion de uikit
            UIkit.notify({
                message: 'Empresa duplicada',
                status: 'danger',
                timeout: 5000,
                pos: 'top-center'
            });
        });
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.borrarEmp = function ($index) {

        console.log($scope.Empresa[$index].id);
        UIkit.modal.confirm('Estas seguro(a)?', function () {

            resourceEmpresa.deleteEmpresa($scope.Empresa[$index].id).ejecutar(function (success) {
                $scope.obtenerEmpresas();
                $scope.reloadRoute();
                //Notificacion de uikit
                UIkit.notify({
                    message: success.response,
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
            }, function (error) {
                $scope.reloadRoute();
                console.log(error);
                //Notificacion de uikit
                UIkit.notify({
                    message: success.response,
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
            })
           // UIkit.modal.alert('Eliminado!');
        });
    };

    $scope.modificar = function ($index) {

        $scope.rif = $scope.Empresa[$index].id;
        $scope.nombre_m = $scope.Empresa[$index].nombre;
        $scope.dir_m = $scope.Empresa[$index].direccion;

    };

    $scope.actEmpresa = function () {
        $scope.empresaMod = {
            "empresa": {
                "id": $scope.rif,
                "nombre": $scope.nombre_m,
                "direccion": $scope.dir_m
            }
        };
        $scope.empresaModJSON = JSON.stringify($scope.empresaMod);
        resourceEmpresa.updateEmpresa($scope.rif).ejecutar($scope.empresaModJSON, function (success) {
            $scope.obtenerEmpresas();
            UIkit.notify({
                message: 'Empresa actualizada',
                status: 'success',
                timeout: 3000,
                pos: 'top-center'
            });
        }, function (error) {
            console.log(error);
            UIkit.notify({
                message: 'Error al actualizar la empresa',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            $.unblockUI();
        });

    };

});