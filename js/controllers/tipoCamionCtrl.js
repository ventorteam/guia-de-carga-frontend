/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.controller('tipoCamionCtrl', function ($scope, resourceTipoCamion, $route) {

    resourceTipoCamion.getAllTipoCamiones().ejecutar(function (data) {
        $scope.tcamion = data.Camiones;
        console.log(data.Camiones);
    }, function (error) {
        console.log(error);
    });

    $scope.recargarTipoCamion = function () {
        resourceTipoCamion.getAllTipoCamiones().ejecutar(function (data) {
            $scope.tcamion = data.Camiones;
            console.log(data.Camiones);
        }, function (error) {
            console.log(error);
        });
    };
    $scope.vaciarTipoCamion = function () {

        $scope.tipo = "";
        $scope.capmin = "";
        $scope.capmax = "";

    };

    $scope.posTipoCamion = function () {

        $scope.datosTipoCamion = {
            "tipocamion": {
                "tipo": $scope.tipo,
                "capmin": $scope.capmin,
                "capmax": $scope.capmax
            }
        };
        $scope.tcamionjson = JSON.stringify($scope.datosTipoCamion);
        console.log($scope.datosTipoCamion);

        if ($scope.capmin < $scope.capmax) {
            resourceTipoCamion.postTipoCamion().ejecutar($scope.tcamionjson, function (data) {

                $scope.recargarTipoCamion();
                $scope.vaciarTipoCamion();
            }, function (error) {

                // alert("Ha ocurrido un error. Tipo de Camion duplicado");
                UIkit.notify({
                    message: 'Ha ocurrido un error. Tipo de Camion duplicado',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });

                $scope.reloadRoute();
                console.log(error);
            });
        } else {
            //alert("Error, capacidad minima supera a la maxima");
            UIkit.notify({
                message: 'Error, capacidad minima supera a la maxima',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.reloadRoute();
        }
    };

    $scope.modificartipo = function ($index) {

        $scope.tipo = $scope.tcamion[$index].tipo;
        $scope.capmin_mod = $scope.tcamion[$index].capmin;
        $scope.capmax_mod = $scope.tcamion[$index].capmax;
        console.log($scope.capmin_mod);
        console.log($scope.capmax_mod);
    };


    $scope.updateTipoCamion = function ($index) {

        $scope.datosTipoCamion_mod = {
            "tipocamion": {
                "tipo": $scope.tipo,
                "capmin": $scope.capmin_mod,
                "capmax": $scope.capmax_mod

            }
        };
        $scope.TipoCamion_modjson = JSON.stringify($scope.datosTipoCamion_mod);
        console.log($scope.datosTipoCamion_mod);

        if ($scope.capmin_mod < $scope.capmax_mod) {
            resourceTipoCamion.updateTipoCamion($scope.tipo).ejecutar($scope.TipoCamion_modjson, function (success) {
                UIkit.notify({
                    message: 'Tipo de camion actualizado con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.recargarTipoCamion();
            }, function (error) {
                UIkit.notify({
                    message: 'Error al actualizar tipo de camion',
                    status: 'danger',
                    timeout: 3000,
                    pos: 'top-center'
                });
                console.log(error);
            });
        } else {
           // alert("Error, capacidad minima supera a la maxima");
            UIkit.notify({
                message: 'Error, capacidad minima supera a la maxima',
                status: 'danger',
                timeout: 3000,
                pos: 'top-center'
            });
            $scope.reloadRoute();
        }
    };

    $scope.reloadRoute = function () {
        $route.reload();
    };


    $scope.deleteTipoCamion = function ($index) {

        UIkit.modal.confirm('Estas seguro(a)?', function () {
            // console.log($scope.dueno[$index].id);
            resourceTipoCamion.deleteTipoCamion($scope.tcamion[$index].tipo).ejecutar(function (success) {
                UIkit.notify({
                    message: 'Tipo de camion eliminado con exito',
                    status: 'success',
                    timeout: 3000,
                    pos: 'top-center'
                });
                $scope.recargarTipoCamion();
                $scope.reloadRoute();
            }, function (error) {
                console.log(error);

            });

        });
    };

});
